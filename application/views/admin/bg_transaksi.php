<?php
    function rupiah($angka){
    
    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
    return $hasil_rupiah;
 
}
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $data_config['nama_app'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>upload/icon/<?php echo $data_config['icon_app'] ?>" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />
    <?php $this->load->view('css') ?>

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row hidden-xs hidden-sm">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url() ?>index.php/admin?home=1">
                                    Home
                                </a>
                            </li>
                            <li class="active">
                                Kasir
                            </li>
                        </ol>
                        <br>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
               <div class="row">
                    <div class="col-md-6">
                        
                        <br>
                        
                        <?php echo $this->session->flashdata('info'); ?>
                        <div class="panel panel-default col-md-12" style="background-color: #F0FFF0;">
                            <br>
                                <div class="table-responsive">
                                    <h4 align="center"><img width="30px" src="<?php echo base_url() ?>vendor/assets/images/icon/shopping-cart.png"> <b> &nbsp;&nbsp;KERANJANG PEMBELIAN</b></h4>
                                    <hr>
                                    <table class="table table-striped table-hover table-cart" id="#sample-table-2">
                                        <thead>
                                            <tr>
                                                <th width="100" class="col-to-export">ID</th>
                                                <th width="140" class="col-to-export">Produk / Item</th>
                                                <th width="140" class="col-to-export">Harga Satuan</th>
                                                <th width="140" class="col-to-export">QTY</th>
                                                <th width="140" class="col-to-export">Jumlah</th>
                                                <th width="1" class="center">#</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th width="100" colspan="3" class="col-to-export"><font size="5">TOTAL</font></th>
                                                <th width="140" colspan="3" class="col-to-export">
                                            <font size="5"><?php echo rupiah($total_belanja['sum']) ?></font></th>
                                            
                                        </tfoot>
                                    </table>
                                </div>
                                <br>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="alert alert-warning hidden-lg hidden-md hidden-xl">
                           <font color="black">
                              <h3>
                                 <b>Sisa masa server 
                                 <?php 
                                    $tgl1 = new DateTime(date('Y-m-d'));
                                    $tgl2 = new DateTime($data_config['end_hosting']);
                                    $jarak = $tgl2->diff($tgl1);

                                    echo $jarak->days;
                                 ?>
                                 hari lagi</b>
                              </h3>
                              Mulai server : <?php echo date('d F Y', strtotime($data_config['start_hosting'])) ?> <br>Berakhir server : <?php echo date('d F Y', strtotime($data_config['end_hosting'])) ?>
                           </font>

                        </div> 
                        <!--<div class="alert alert-warning hidden-xs hidden-sm">
                            <b>Warning !</b> - Silahkan gunakan <b>seacrh engine</b> pada sisi kanan tabel dibawah untuk pencarian lebih cepat
                        </div>-->
                        <br>
                         <div class="panel panel-default col-md-12" style="background-color: #FFFACD;">
                            <div class="panel-body">
                                <div class="row">
                                    <form role="form" action="<?php echo base_url();?>index.php/admin/addtocart" method="post">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <p class="hidden-xs hidden-sm" align="center"><img width="50%px" src="<?php echo base_url() ?>vendor/assets/images/icon/cashier-machine.png">
                                                            </p>
                                                           <!-- <a style="
                                                                box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                padding: 10px; 
                                                                border: 10px dash grey;" 
                                                                class="btn btn-icon btn-block">
                                                                <h3><b><?php echo $total_produk['count'] ?> produk</b></h3>
                                                                <span class="badge badge-yellow" 
                                                                style="
                                                                    box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                    padding: 10px; 
                                                                    border: 1px grey;" >
                                                                <font color="black"><b>TOTAL PRODUK</b> DI KERANJANG</font></span>
                                                            </a>
                                                            <p align="center"><font> KET: Jumlah produk dikeranjang</font></p> -->
                                                        </div>
                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <p align="center"><font size="6">POS KASIR I</font></p>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                    <label>Pilih Produk / Item:</label>
                                                    <p>
                                                        <select
                                                            name="cart_item"
                                                            class="form-control"
                                                            required>
                                                            <option value="">
                                                                Pilih Produk
                                                            </option>
                                                            <?php
                                                                foreach($data_barang->result_array() as $d)
                                                                {
                                                                    echo '<option value='.$d['barang_no'].'>['.$d['kategori_nama'].'] - 
                                                                            '.$d['barang_nama'].' - ('.$d['barang_stok'].' '.$d['barang_satuan'].')
                                                                        </option>';
                                                                }
                                                            ?>
                                                        </select>
                                                    </p>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Kuantitas:</label>
                                                    <p>
                                                        <input 
                                                            type="number" 
                                                            name="cart_qty"
                                                            class="form-control"
                                                            placeholder="Ex : 2" 
                                                            required>
                                                    </p>
                                                </div>
                                                <div class="col-md-12">
                                                    <br><br>
                                                    <button class="btn btn-red space10" type="submit" class="btn btn-red" 
                                                    style="
                                                        box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                        padding: 10px; 
                                                        border: 1px grey;
                                                        width: 100%;">
                                                        ADD <br>TO CART
                                                    </button>
                                                </div>
                                                <br>
                                                <!--<div class="col-md-6">
                                                    
                                                    <button data-toggle="modal" data-target="#modalBayar" type="submit" class="btn btn-green" 
                                                    style="
                                                        box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                        padding: 10px; 
                                                        border: 1px grey;
                                                        width: 100%;">
                                                        BAYAR <br>CASH
                                                    </button>
                                                </div>-->
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <!--<div class="alert alert-warning hidden-xs hidden-sm">
                            <b>Warning !</b> - Silahkan gunakan <b>seacrh engine</b> pada sisi kanan tabel dibawah untuk pencarian lebih cepat
                        </div>-->
                        <br>
                        <div class="alert alert-warning hidden-lg hidden-md hidden-xl">
                           <font color="black">
                              <h3>
                                 <b>Sisa masa server 
                                 <?php 
                                    $tgl1 = new DateTime(date('Y-m-d'));
                                    $tgl2 = new DateTime($data_config['end_hosting']);
                                    $jarak = $tgl2->diff($tgl1);

                                    echo $jarak->days;
                                 ?>
                                 hari lagi</b>
                              </h3>
                              Mulai server : <?php echo date('d F Y', strtotime($data_config['start_hosting'])) ?> <br>Berakhir server : <?php echo date('d F Y', strtotime($data_config['end_hosting'])) ?>
                           </font>

                        </div> 
                         <div class="panel panel-default col-md-12" style="background-color: #FFFACD;">
                            <div class="panel-body">
                                <div class="row">
                                    <form role="form" action="<?php echo base_url();?>index.php/admin/addtocart_2" method="post">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12">
                                                    <div class="row">
                                                        
                                                        <div class="col-sm-12">
                                                            <p class="hidden-xs hidden-sm" align="center"><img width="50%px" src="<?php echo base_url() ?>vendor/assets/images/icon/cashier-machine.png">
                                                            </p>
                                                            <!--<a style="
                                                                box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                padding: 10px; 
                                                                border: 10px dash grey;" 
                                                                class="btn btn-icon btn-block">
                                                                <h3><b><?php echo rupiah($total_belanja['sum']) ?></b></h3>
                                                                <span class="badge badge-danger"
                                                                style="
                                                                    box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                    padding: 10px; 
                                                                    border: 1px grey;">
                                                                <b>TOTAL BIAYA BELANJA</b></span>
                                                            </a>
                                                            <p align="center"><font> KET: Jumlah harus dibayar</font></p>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <p align="center"><font size="6">POS KASIR II</font></p>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                    <label>Ketik Produk / Item:</label>
                                                    <p>
                                                        <textarea 
                                                            name="cart_item"
                                                            class="form-control" 
                                                            placeholder="Ex : Perawatan Luka Ringan + Hydrocolloid 1/4" 
                                                            required></textarea>
                                                    </p>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Price:</label>
                                                    <p>
                                                        <input 
                                                            type="text" 
                                                            name="cart_harga"
                                                            placeholder="Ex : 100000" 
                                                            class="form-control uang" 
                                                            required>
                                                    </p>
                                                </div>
                                                <div class="col-md-6">
                                                    <br>
                                                    <button class="btn btn-red" type="submit" class="btn btn-red" 
                                                    style="
                                                        box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                        padding: 10px; 
                                                        border: 1px grey;
                                                        width: 100%;">
                                                        ADD <br>TO CART
                                                    </button>
                                                </div>
                                                <div class="col-md-6">
                                                    <br>
                                                    <button data-toggle="modal" data-target="#modalBayar" type="submit" class="btn btn-green" 
                                                    style="
                                                        box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                        padding: 10px; 
                                                        border: 1px grey;
                                                        width: 100%;">
                                                        BAYAR <br>CASH
                                                    </button>
                                                </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
            <!-- MODAL TAMBAH DATA -->
            <?php echo $modalBayar; ?>
            <!-- END MODAL TAMBAH DATA -->
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['versi'] ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
    	<!-- ISI RIGHT SIDE BAR -->
    <!-- end: RIGHT SIDEBAR -->
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>
    
    
    <script src="<?php echo base_url();?>vendor/bower_components/twbs-pagination/jquery.twbsPagination.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.pulsate/jquery.pulsate.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-elements.min.js"></script>

    <!--BUTTON-->
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/spin.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/jquery.mask.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <?php $this->load->view('admin/menu_bawah'); ?>
    <?php echo $this->session->flashdata('info2'); ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
           // TableExport.init();
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){

            // Format mata uang.
            $( '.uang' ).mask('000.000.000', {reverse: true});

        })
    </script>


    <script type="text/javascript">

        $(".table-cart").DataTable({
            "iDisplayLength": 7,
            ordering: false,
            processing: true,
            serverSide: true,
            ajax: {
              url: "<?php echo base_url('index.php/admin/data_cart') ?>",
              type:'POST',
            }
        });

    </script>

    <script type="text/javascript">
      $('select').select2();
    </script>

</body>

</html>