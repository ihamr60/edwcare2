<ul class="main-navigation-menu">
    
     <?php 
        if($status == 'Admin Klinik')
        {
    ?>
    <li>
        <a 
            <?php if (isset($_GET['home']) && $_GET['home']==1){echo 'style="background: #DC143C; color: white;"';}?> 
            href="<?php echo base_url();?>index.php/admin?home=1">
            <i class="fa fa-square-o" <?php if (isset($_GET['home']) && $_GET['home']==1){echo 'style="background: #DC143C; color: white;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">HOME</span>
        </a>
    </li>
<?php } ?>
    <li>
        <a 
            <?php if (isset($_GET['transaksi']) && $_GET['transaksi']==1){echo 'style="background: #DC143C; color: white;"';}?> 
            href="<?php echo base_url();?>index.php/admin/bg_transaksi?transaksi=1">
            <i class="fa fa-square-o" <?php if (isset($_GET['transaksi']) && $_GET['transaksi']==1){echo 'style="background: #DC143C; color: white;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">KASIR</span> 
        </a>
    </li>  
    <li>
        <a 
            <?php if (isset($_GET['pass']) && $_GET['pass']==1){echo 'style="background: #DC143C; color: white;"';}?> 
            href="<?php echo base_url();?>index.php/admin/bg_password?pass=1">
            <i class="fa fa-square-o" <?php if (isset($_GET['pass']) && $_GET['pass']==1){echo 'style="background: #DC143C; color: white;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">GANTI PASSWORD</span> 
        </a>
    </li> 
    <li>
        <a 
            <?php if (isset($_GET['barang']) && $_GET['barang']==1){echo 'style="background: #DC143C; color: white;"';}?> 
            href="<?php echo base_url();?>index.php/admin/bg_barang?barang=1">
            <i class="fa fa-square-o" <?php if (isset($_GET['barang']) && $_GET['barang']==1){echo 'style="background: #DC143C; color: white;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">
               <?php 
                if($status == 'Admin Klinik')
                {
                    echo 'MANAJEMEN BARANG';
                }
                else
                {
                    echo 'PENGELUARAN';
                }
               ?>
                
            </span> 
        </a>
    </li> 
    <li>
        <a 
            <?php if (isset($_GET['transaksi2']) && $_GET['transaksi2']==1){echo 'style="background: #DC143C; color: white;"';}?> 
            href="<?php echo base_url();?>index.php/admin/bg_riwayat_transaksi/0/<?php echo date('Y-m-d')."/".date('Y-m-d') ?>?transaksi2=1">
            <i class="fa fa-square-o" <?php if (isset($_GET['transaksi2']) && $_GET['transaksi2']==1){echo 'style="background: #DC143C; color: white;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">TRANSAKSI</span>
        </a>
    </li>
    
    <!--<li>
        <a 
            <?php if (isset($_GET['user']) && $_GET['user']==1){echo 'style="background: #DC143C; color: white;"';}?> 
            href="<?php echo base_url();?>index.php/admin/bg_user?user=1">
            <i class="fa fa-square-o" <?php if (isset($_GET['user']) && $_GET['user']==1){echo 'style="background: #DC143C; color: white;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">DATA USER</span>
        </a>
    </li>-->
    <li>
        <a 
            <?php if (isset($_GET['server']) && $_GET['server']==1){echo 'style="background: #DC143C; color: white;"';}?> 
            href="<?php echo base_url();?>index.php/admin/bg_tagihan_server?server=1">
            <i class="fa fa-square-o" <?php if (isset($_GET['server']) && $_GET['server']==1){echo 'style="background: #DC143C; color: white;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">TAGIHAN SERVER</span>
        </a>
    </li>
    <li>
        <a 
            <?php if (isset($_GET['pasien']) && $_GET['pasien']==1){echo 'style="background: #DC143C; color: white;"';}?> 
            href="<?php echo base_url();?>index.php/admin/bg_pasien/0?pasien=1">
            <i class="fa fa-square-o" <?php if (isset($_GET['pasien']) && $_GET['pasien']==1){echo 'style="background: #DC143C; color: white;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">DATA PASIEN</span> 
        </a>
    </li>
    <li>
        <a
            href="<?php echo base_url();?>index.php/welcome/logout/">
            <i class="fa fa-square-o" style="color: black;" ></i>
            <span class="title">LOGOUT</span>
        </a>
    </li>   

</ul>
<div class="alert alert-warning">
                               <font color="black">
                                  <h3>
                                     Masa server akan berakhir dalam waktu
                                     <?php 
                                        $tgl1 = new DateTime(date('Y-m-d'));
                                        $tgl2 = new DateTime($data_config['end_hosting']);
                                        $jarak = $tgl2->diff($tgl1);

                                        echo $jarak->days;
                                     ?>
                                     hari
                                     <br>
                                     
                                  </h3>
                                  Mulai server : <br><?php echo date('d F Y', strtotime($data_config['start_hosting'])) ?> <br><br>Berakhir server : <br><?php echo date('d F Y', strtotime($data_config['end_hosting'])) ?>
                               </font>

                            </div>