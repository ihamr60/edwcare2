<?php 
    function rupiah($angka){
            $hasil_rupiah = "" . number_format($angka,0,',','.');
            return $hasil_rupiah;
        }
?>

<html>
<head>
<title>Faktur Pembayaran</title>
<style>
 
#tabel
{
font-size:15px;
border-collapse:collapse;
}
#tabel  td
{
padding-left:5px;
border: 1px solid black;
}
</style>
</head>
<body style='font-family:tahoma; font-size:13pt;'>
<center><table style='width:350px; font-size:23pt; font-family:calibri; border-collapse: collapse;' border = '0'>
<td width='100%' align='CENTER'><span style='color:black; vertical-align:top;'>
<b><font size='7'><u>KLINIK EDWCARE</u></font></b></br>JL A.Yani depan Suzuya Mall </span></br>
 
 
<span style='font-size:20pt'><?php echo date('d M Y', strtotime(date('Y-m-d'))) ?>, <?php echo date('H:i', strtotime(date('H:i'))) ?> WIB</span></br>
</td>
</table>
<style>
hr { 
    display: block;
    margin-top: 0.5em;
    margin-bottom: 0.5em;
    margin-left: 0px;
    margin-right: 0px;
    border-style: inset;
    border: 2px dashed black;
} 
</style>
<br>
<table cellspacing='0' cellpadding='0' style='width:350px; font-size:17pt; font-family:calibri;  border-collapse: collapse;' border='0'>
 
<tr align='center'>
    <td width='10%'><font size='5'><b>ITEM</b></font></td>
    <td width='13%'><font size='5'><b>PRICE</b></td>
    <td width='4%'><font size='5'><b>QTY</b></td>
    <td width='13%'><font size='5'><b>TOTAL</b></td><tr>
    <td colspan='5'><hr></td></tr>
</tr>
<?php 
    foreach($data_cart->result_array() as $d)
    {
        if(empty($d['barang_nama']))
            {
                $barang = $d['cart_item'];
            }
            else
            {
                $barang = $d['barang_nama'];
            }
?>
<tr>
    <td style='vertical-align:top;'><?php echo $barang ?></td>
    <td style='vertical-align:top; text-align:right; padding-right:10px'><b><?php echo rupiah($d['cart_harga']) ?></b></td>
    <td style='vertical-align:top; text-align:right; padding-right:10px'><b><?php echo $d['cart_qty'] ?></b></td>
    <td style='text-align:right; vertical-align:top'><b><?php echo rupiah($d['cart_jumlah']) ?></b></td>
</tr>
<td colspan='5'><hr></td>
<?php } ?>
</tr>
<tr>
<td colspan = '3'><div style='text-align:right; color:black'>TOTAL : </div></td><td style='text-align:right; font-size:16pt; color:black'><b><?php echo rupiah($total_belanja['transaksi_total']) ?></b></td>
</tr>
<tr>
<td colspan = '3'><div style='text-align:right; color:black'>CASH : </div></td><td style='text-align:right; font-size:16pt; color:black'><b><?php echo rupiah($total_belanja['transaksi_bayar']) ?></b></td>
</tr>
<tr>
<td colspan = '3'><div style='text-align:right; color:black'>KEMBALI : </div></td><td style='text-align:right; font-size:16pt; color:black'><b><?php echo rupiah($total_belanja['transaksi_kembalian']) ?></b></td>
</tr>
</table>
<table style='width:350; font-size:20pt;' cellspacing='2'><tr><br>Semoga Anda lekas sembuh :)</br><td align='center'>**** TERIMAKASIH ****</td></tr></table></center></body>
</html>