<div id="modalTambahBarang" class="modal fade" data-width="560">
    <form role="form" action="<?php echo base_url();?>index.php/admin/tambahBarang" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>

            <h4 class="modal-title"> <img width="30px" src="<?php echo base_url()?>vendor/assets/images/icon/hospital.png">&nbsp;&nbsp;&nbsp;FORM TAMBAH BARANG</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Nama Barang / Produk:</label>
                    <p>
                        <input
                            type="text"
                            name="barang_nama"
                            class="form-control"
                            placeholder="Ex : Antasida Doen"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>Stok:</label>
                    <p>
                        <input
                            type="number"
                            name="barang_stok"
                            class="form-control"
                            value="0"
                            readonly
                            required>
                    </p>
                    <font size="" color="red"><b>Note:</b> Pengisian stok barang harap dilakukan pada <b>tabel pembelian stok barang</b> agar terdata dalam database pengeluaran</font>
                   <br><br>
                </div>
                
                
                <div class="col-md-12">
                    <label>Kategori:</label>
                    <p>
                        <select
                            id="select2insidemodal"
                            name="barang_kategori"
                            class="form-control"
                            required>
                            <option value="">
                                Please Select
                            </option>
                            <?php 
                                foreach($data_kategori->result_array() as $d)
                                {
                                    echo '<option value="'.$d['kategori_no'].'">
                                                '.$d['kategori_nama'].'
                                            </option>';
                                }
                            ?>
                        </select>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>Harga Jual Satuan:</label>
                    <p>
                        <input
                            type="text"
                            name="barang_harga"
                            class="form-control uang"
                            placeholder="Ex : 200000"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>Satuan:</label>
                    <p>
                        <input
                            type="text"
                            name="barang_satuan"
                            class="form-control"
                            placeholder="Ex : Pcs"
                            required>
                    </p>
                   
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>