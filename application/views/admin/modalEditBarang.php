<?php 
    foreach($data_barang->result_array() as $d)
    {
?>
<div id="modalEditBarang<?php echo $d['barang_no'] ?>" class="modal fade" data-width="560">
    <form role="form" action="<?php echo base_url();?>index.php/admin/editBarang" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>

            <h4 class="modal-title"> <img width="30px" src="<?php echo base_url()?>vendor/assets/images/icon/hospital.png">&nbsp;&nbsp;&nbsp;FORM TAMBAH BARANG</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Nama Barang / Produk:</label>
                    <p>
                        <input type="hidden" value="<?php echo $d['barang_no'] ?>" name="barang_no">
                        <input
                            type="text"
                            name="barang_nama"
                            class="form-control"
                            placeholder="Ex : Antasida Doen"
                            value="<?php echo $d['barang_nama'] ?>"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>Stok:</label>
                    <p>
                        <input
                            type="number"
                            name="barang_stok"
                            class="form-control"
                            placeholder="Ex : 100"
                            value="<?php echo $d['barang_stok'] ?>"
                            readonly
                            required>
                    </p>
                   
                </div>
                
                <div class="col-md-12">
                    <label>Kategori:</label>
                    <p>
                        <select
                            id="select2insidemodal2"
                            name="barang_kategori"
                            class="form-control"
                            required>
                            <option value="">
                                Please Select
                            </option>
                            <?php 
                                foreach($data_kategori->result_array() as $e)
                                {
                                    if($d['barang_kategori'] == $e['kategori_no'])
                                    {
                                        echo '<option selected value="'.$e['kategori_no'].'">
                                                '.$e['kategori_nama'].'
                                            </option>';
                                    }
                                    else
                                    {
                                        echo '<option value="'.$e['kategori_no'].'">
                                                '.$e['kategori_nama'].'
                                            </option>';
                                    }
                                }
                            ?>
                        </select>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>Harga Jual Satuan:</label>
                    <p>
                        <input
                            type="text"
                            name="barang_harga"
                            class="form-control uang"
                            placeholder="Ex : 200000"
                            value="<?php echo $d['barang_harga'] ?>"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>Satuan:</label>
                    <p>
                        <input
                            type="text"
                            name="barang_satuan"
                            class="form-control"
                            placeholder="Ex : Pcs"
                            value="<?php echo $d['barang_satuan'] ?>"
                            required>
                    </p>
                   
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Update
            </button>
        </div>
    </form>
</div>

<?php } ?>