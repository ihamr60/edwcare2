<?php
    function rupiah($angka){
    
    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
    return $hasil_rupiah;
 
}
?>

<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $data_config['nama_app'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>upload/icon/<?php echo $data_config['icon_app'] ?>" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />
    <?php $this->load->view('css') ?>

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row hidden-xs hidden-sm">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url() ?>">
                                    Home
                                </a>
                            </li>
                            <li class="active">
                                Riwayat Transaksi
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php //echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
               <div class="row">
                    <div class="col-md-12">
                        <br>
                        <?php echo $this->session->flashdata('info'); ?>
                        <div class="alert alert-warning hidden-lg hidden-md hidden-xl">
                           <font color="black">
                              <h3>
                                 <b>Sisa masa server 
                                 <?php 
                                    $tgl1 = new DateTime(date('Y-m-d'));
                                    $tgl2 = new DateTime($data_config['end_hosting']);
                                    $jarak = $tgl2->diff($tgl1);

                                    echo $jarak->days;
                                 ?>
                                 hari lagi</b>
                              </h3>
                              Mulai server : <?php echo date('d F Y', strtotime($data_config['start_hosting'])) ?> <br>Berakhir server : <?php echo date('d F Y', strtotime($data_config['end_hosting'])) ?>
                           </font>

                        </div> 
                        <!--<div class="alert alert-warning hidden-xs hidden-sm">
                            <b>Warning !</b> - Silahkan gunakan <b>seacrh engine</b> pada sisi kanan tabel dibawah untuk pencarian lebih cepat
                        </div>-->
                         <div class="panel panel-default col-md-12" style="background-color: #FFFACD;">
                            <div class="panel-body">
                                <h4 align=""><img width="25px" src="<?php echo base_url() ?>vendor/assets/images/icon/search.png"> <b> &nbsp;&nbsp;MESIN PENCARIAN TANGGAL</b></h4>
                                    <hr>
                                <div class="row">
                                    <form role="form" action="<?php echo base_url();?>index.php/admin/filter_transaksi" method="post">
                                        
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <label><b>DARI TANGGAL :</b></label>
                                                    <p>
                                                        <input
                                                            type="date"
                                                            name="tgl_awal"
                                                            class="form-control"
                                                            value="<?php echo $this->uri->segment(4) ?>"
                                                            required>
                                                    </p>
                                                   
                                                </div>
                                                <div class="col-md-5">
                                                    <label><b>SAMPAI TANGGAL :</b></label>
                                                    <p>
                                                        <input
                                                            type="date"
                                                            name="tgl_akhir"
                                                            value="<?php echo $this->uri->segment(5) ?>"
                                                            class="form-control"
                                                            required>
                                                    </p>
                                                   
                                                </div>
                                                <div class="col-md-2">
                                                    <label>FILTER</label>
                                                    <button type="submit" style="width: 100%" class="btn btn-yellow">
                                                        <font color="black"><b>TERAPKAN</b></font>
                                                    </button>
                                                   
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <?php echo $this->session->flashdata('info'); ?>
                        <!--<div class="alert alert-warning hidden-xs hidden-sm">
                            <b>Warning !</b> - Silahkan gunakan <b>seacrh engine</b> pada sisi kanan tabel dibawah untuk pencarian lebih cepat
                        </div>-->
                        <div class="panel panel-default" style="background-color: #F0FFF0;">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <h4 align=""><img width="25px" src="<?php echo base_url() ?>vendor/assets/images/icon/fees.png"> <b> &nbsp;&nbsp;DATABASE TRANSAKSI KASIR </b></h4>
                                    <hr>

                                    <div class="col-md-8">
                                        <a 
                                        style="
                                                box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                padding: 10px; 
                                                border: 1px grey;
                                                width: 100%;"
                                                target="_blank" href="<?php echo base_url() ?>index.php/admin/bg_cetak_riwayat_transaksi/<?php echo $this->uri->segment(4) ?>/<?php echo $this->uri->segment(5) ?>" type="submit" style="width: 100%" class="btn btn-blue">
                                            <img width="20px" src="<?php echo base_url() ?>vendor/assets/images/web/printer.png"> 
                                            <font color="white"><b> PRINT RINCIAN ( FROM <?php echo date('d/m/Y', strtotime($this->uri->segment(4))) ?> S/D <?php echo date('d/m/Y', strtotime($this->uri->segment(5))) ?> )</b></font>
                                        </a>
                                    </div>
                                    <br><br><br>
                                    <table class="table table-striped table-hover table-transaksi" id="#sample-table-2">
                                        <thead>
                                            <tr>
                                                <th width="100" class="col-to-export">WAKTU</th>
                                                <th width="140" class="col-to-export">KODE TRANSAKSI</th>
                                                <th width="140" class="col-to-export">NOMINAL</th>
                                                <th width="120" class="center">ACTION</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th width="100" colspan="2" class="col-to-export"><font size="4">TOTAL</font></th>
                                                <th width="120" colspan="2" class="center"><font size="4">
                                                    <?php 
                                                        if($status == 'Admin Klinik')
                                                        {
                                                            echo rupiah($total_transaksi_harini['sum']);
                                                        }
                                                        else
                                                        {
                                                            echo '****';
                                                        }
                                                    ?>
                                                </font></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <br>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-5" style="position: sticky; top: 70px;">
                        <?php echo $this->session->flashdata('info'); ?>
                      <!--  <div class="alert alert-warning">
                               <font color="black">
                                  <h3>
                                     Sisa masa server 
                                     <?php 
                                        $tgl1 = new DateTime($data_config['start_hosting']);
                                        $tgl2 = new DateTime($data_config['end_hosting']);
                                        $jarak = $tgl2->diff($tgl1);

                                        echo $jarak->days;
                                     ?>
                                     hari lagi
                                     <br>
                                     
                                  </h3>
                                  Mulai server : <?php echo date('d F Y', strtotime($data_config['start_hosting'])) ?> <br>Berakhir server : <?php echo date('d F Y', strtotime($data_config['end_hosting'])) ?>
                               </font>

                            </div> -->
                        <!--<div class="alert alert-warning hidden-xs hidden-sm">
                            <b>Warning !</b> - Silahkan gunakan <b>seacrh engine</b> pada sisi kanan tabel dibawah untuk pencarian lebih cepat
                        </div>-->
                        <div class="panel panel-default" style="background-color: #F0FFF0;">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <h4 align=""><img width="25px" src="<?php echo base_url() ?>vendor/assets/images/icon/checklist.png"> <b> &nbsp;&nbsp;DETAIL PRODUK TERJUAL</b></h4>
                                    <hr>
                                    <table class="table table-striped table-hover table-produk" id="#sample-table-2">
                                        <thead>
                                            <tr>
                                                <th width="1" class="col-to-export">NO</th>
                                                <th width="100" class="col-to-export">PRODUK</th>
                                                <th width="140" class="col-to-export">HARGA</th>
                                                <th width="1" class="col-to-export">QTY</th>
                                                <th width="140" class="col-to-export">TOTAL</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                                <th width="100" colspan="3" class="col-to-export"><font size="4">TOTAL</font></th>
                                                <th width="140" colspan="2" class="col-to-export">
                                                    <font size="4">
                                                        <?php 
                                                        if(!empty($total_transaksi['transaksi_total']))
                                                        {
                                                            if($status == 'Admin Klinik')
                                                            {
                                                                echo rupiah($total_transaksi['transaksi_total']);
                                                            }
                                                            else
                                                            {
                                                                echo '****';
                                                            }
                                                        }
                                                        else
                                                        {
                                                            echo "Rp. 0";   
                                                        }
                                                    ?>
                                                    </font>
                                                </th>
                                        </tfoot>
                                    </table>
                                    <br>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
            <!-- MODAL TAMBAH DATA -->
            <?php //echo $modalTambahPasien; ?>
            <!-- END MODAL TAMBAH DATA -->
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['versi'] ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
    	<!-- ISI RIGHT SIDE BAR -->
    <!-- end: RIGHT SIDEBAR -->
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>
    
    
    <script src="<?php echo base_url();?>vendor/bower_components/twbs-pagination/jquery.twbsPagination.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.pulsate/jquery.pulsate.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-elements.min.js"></script>

    <!--BUTTON-->
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/spin.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-buttons.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <?php $this->load->view('admin/menu_bawah'); ?>
    <?php echo $this->session->flashdata('info2'); ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
           // TableExport.init();
        });
    </script>

    <script type="text/javascript">

        $(".table-transaksi").DataTable({
            "iDisplayLength": 1000,
            processing: true,
            serverSide: true,
            ajax: {
              url: "<?php echo base_url('index.php/admin/data_riwayat_transaksi_optimized/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5).'') ?>",
              type:'POST',
            },
            columnDefs: [
                {
                    "targets": 3,
                    "orderable": false
                }
            ],
            order: [
                [0, "desc" ]
            ],
        });


        $(".table-produk").DataTable({
            "iDisplayLength": 7,
            dom: 'Bfrtip',
                buttons: [
                    'print'
                ],
            "searching":false,
            ordering: false,
            processing: true,
            serverSide: true,
            ajax: {
              url: "<?php echo base_url('index.php/admin/data_produk_terjual/'.$this->uri->segment(3).'') ?>",
              type:'POST',
            }
        });

    </script>

</body>

</html>