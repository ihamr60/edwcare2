<div id="modalTambahPembelianManual" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
    <form role="form" action="<?php echo base_url();?>index.php/admin/tambahPembelianManual" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>

            <h4 class="modal-title"> <img width="30px" src="<?php echo base_url()?>vendor/assets/images/icon/hospital.png">&nbsp;&nbsp;&nbsp;FORM CATATAN PEMBELIAN MANUAL</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>Tanggal Beli / Pengeluaran:</label>
                    <p>
                        <input
                            type="date"
                            name="keluar_date"
                            class="form-control"
                            value="<?php echo date('Y-m-d') ?>"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>Nama Barang / Jenis Pengeluaran:</label>
                    <p>
                        <input
                            type="text"
                            name="keluar_produk"
                            class="form-control"
                            pattern="[a-zA-Z0-9 ]+"
                            placeholder="Ex : Antasida Doen / Biaya makan staff, dll"
                            required>
                            <font size="0" color="red">NOTE: Hanya gunakan huruf & angka saja (tidak dianjurkan menggunakan simbol dan tanda baca lainnya)</font>
                    </p>

                   
                </div>
                <div class="col-md-12">
                    <label>Qty:</label>
                    <p>
                        <input
                            type="number"
                            name="keluar_qty"
                            class="form-control"
                            placeholder="Ex : 1"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>Harga Beli:</label>
                    <p>
                        <input
                            type="text"
                            name="keluar_hg_beli"
                            class="form-control uang"
                            placeholder="Ex : 200000"
                            required>
                    </p>
                   
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>