<div id="modalTambahPasien" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
    <form role="form" action="<?php echo base_url();?>index.php/admin/tambahPasien" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>

            <h4 class="modal-title"> <img width="30px" src="<?php echo base_url()?>vendor/assets/images/icon/hospital.png">&nbsp;&nbsp;&nbsp;FORM TAMBAH PASIEN</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>NIK KTP / ID Pasien:</label>
                    <p>
                        <input
                            type="text"
                            name="pasien_nik"
                            class="form-control"
                            minlength="6"
                            placeholder="Ex: 11740212XXXXXXXXXX / ID00232"
                            required>
                            <font size="0">Notice: <b>NIK KTP / ID Pasien</b> dibutuhkan sebagai Primary Key dalam sistem</font>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>Nama Lengkap:</label>
                    <p>
                        <input
                            type="text"
                            name="pasien_nama"
                            class="form-control"
                            placeholder="Ex: Ilham Ramadhan, S. Tr. Kom"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>Tanggal Lahir:</label>
                    <p>
                        <input
                            type="date"
                            name="pasien_tgl_lhr"
                            class="form-control"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>Gender:</label>
                    <p>
                        <select
                            name="pasien_kelamin"
                            class="form-control"
                            required>
                            <option value="">
                                Please Select
                            </option>
                            <option value="Laki-laki">
                                Laki-laki
                            </option>
                            <option value="Perempuan">
                                Perempuan
                            </option>
                        </select>
                    </p>
                   
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>