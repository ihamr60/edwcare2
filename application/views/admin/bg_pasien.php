<?php
function hitung_umur($tanggal_lahir)
        {
            $birthDate = new DateTime($tanggal_lahir);
            $today = new DateTime("today");
            if ($birthDate > $today) 
            { 
                exit("0 tahun 0 bulan 0 hari");
            }
            $y = $today->diff($birthDate)->y;
            $m = $today->diff($birthDate)->m;
            $d = $today->diff($birthDate)->d;
            return $y." tahun ".$m." bulan";
        }
?>

<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $data_config['nama_app'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>upload/icon/<?php echo $data_config['icon_app'] ?>" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />
    <?php $this->load->view('css') ?>

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row hidden-xs hidden-sm">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url() ?>">
                                    Dashboard
                                </a>
                            </li>
                            <li class="active">
                                Data Pasien
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php //echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
               <div class="row">
                    <div class="col-md-6">
                        <br>
                        <?php echo $this->session->flashdata('info'); ?>
                        <div class="alert alert-warning hidden-lg hidden-md hidden-xl">
                           <font color="black">
                              <h3>
                                 <b>Sisa masa server 
                                 <?php 
                                    $tgl1 = new DateTime(date('Y-m-d'));
                                    $tgl2 = new DateTime($data_config['end_hosting']);
                                    $jarak = $tgl2->diff($tgl1);

                                    echo $jarak->days;
                                 ?>
                                 hari lagi</b>
                              </h3>
                              Mulai server : <?php echo date('d F Y', strtotime($data_config['start_hosting'])) ?> <br>Berakhir server : <?php echo date('d F Y', strtotime($data_config['end_hosting'])) ?>
                           </font>

                        </div> 
                        <!--<div class="alert alert-warning hidden-xs hidden-sm">
                            <b>Warning !</b> - Silahkan gunakan <b>seacrh engine</b> pada sisi kanan tabel dibawah untuk pencarian lebih cepat
                        </div>-->
                        <div class="panel panel-default" style="background-color: #F0FFF0;">
                            <div class="panel-body">
                                <h4 align="center"><img width="25px" src="<?php echo base_url() ?>vendor/assets/images/icon/people.png"> <b> &nbsp;&nbsp;DATABASE PASIEN EDWCARE</b></h4>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12 space20">
                                        <a data-toggle="modal" style=' width:100%' data-target="#modalTambahPasien" class="btn btn-yellow">
                                           <font color="black"><i class="fa fa-plus"></i><b> TAMBAHKAN PASIEN BARU </b></font>
                                        </a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover table-pasien" id="#sample-table-2">
                                        <thead>
                                            <tr>
                                                <th width="1" class="col-to-export">JOINED</th> 
                                                <th width="1" class="col-to-export">PASIEN</th>   
                                                <th width="1" class="col-to-export">GENDER</th>
                                                <th width="1" class="col-to-export">TANGGAL LAHIR</th>
                                                <th width="1" class="center">ACTION</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <br>
                        <?php echo $this->session->flashdata('info'); ?>
                        <!--<div class="alert alert-warning">
                               <font color="black">
                                  <h3>
                                     Sisa masa server 
                                     <?php 
                                        $tgl1 = new DateTime($data_config['start_hosting']);
                                        $tgl2 = new DateTime($data_config['end_hosting']);
                                        $jarak = $tgl2->diff($tgl1);

                                        echo $jarak->days;
                                     ?>
                                     hari lagi
                                     <br>
                                     
                                  </h3>
                                  Mulai server : <?php echo date('d F Y', strtotime($data_config['start_hosting'])) ?> <br>Berakhir server : <?php echo date('d F Y', strtotime($data_config['end_hosting'])) ?>
                               </font>

                            </div> -->
                        <!--<div class="alert alert-warning hidden-xs hidden-sm">
                            <b>Warning !</b> - Silahkan gunakan <b>seacrh engine</b> pada sisi kanan tabel dibawah untuk pencarian lebih cepat
                        </div>-->
                        <div class="panel panel-default" style="background-color: #F0FFF0;">
                            <div class="panel-body">
                                <h4 align="center"><img width="25px" src="<?php echo base_url() ?>vendor/assets/images/icon/checklist.png"> <b> &nbsp;&nbsp;CATATAN MEDIS PASIEN EDWCARE</b></h4>
                                <hr>
                                <table align="center">
                                    <body>
                                        <tr>
                                            <td width="" class="col-to-export"><b>NAMA / ID PASIEN :</b></td> 
                                            <td class="col-to-export" width="110px"></td>
                                            <td width="" class=""><b>TANGGAL LAHIR:</b></td>
                                        </tr>
                                        <tr>
                                            <td width=""  class="col-to-export"><span style='text-transform: uppercase;'>
                                                <?php 
                                                    if(!empty($data_pasien['pasien_nama']))
                                                    {
                                                         echo "- ".$data_pasien['pasien_nik']."<br>- ".$data_pasien['pasien_nama'];
                                                    }
                                                ?></span></td> 
                                            <td class="col-to-export" width="40%"></td>
                                            <td width="" class=""> <span style='text-transform: uppercase;'>
                                                <?php 
                                                    if(!empty($data_pasien['pasien_nama']))
                                                    {
                                                        echo "- ".date('d-F-Y', strtotime($data_pasien['pasien_tgl_lhr']))."<br>- ".hitung_umur($data_pasien['pasien_tgl_lhr']);
                                                    } 
                                                ?> </span></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12 space20">
                                        <form role="form" action="<?php echo base_url();?>index.php/admin/tambah_catatan_medis" method="post">
                                        
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label><b>TANGGAL BEROBAT :</b></label>
                                                    <p>
                                                        <input
                                                            type="hidden"
                                                            name="medis_nik"
                                                            class="form-control"
                                                            value="<?php 
                                                                if(!empty($data_pasien['pasien_nik']))
                                                                {
                                                                    echo $data_pasien['pasien_nik'];
                                                                }
                                                            ?>"
                                                            required>
                                                        <input
                                                            type="date"
                                                            name="medis_date"
                                                            class="form-control"
                                                            value="<?php echo date('Y-m-d') ?>"
                                                            required>
                                                    </p>
                                                   
                                                </div>
                                                <div class="col-md-8">
                                                    <label><b>CATATAN MEDIS :</b></label>
                                                    <p>
                                                        <textarea
                                                            name="medis_note"
                                                            placeholder="Ex: Luka bakar dibagian dada / 21 Jahitan dibagian punggung"
                                                            class="form-control"
                                                            required></textarea>
                                                    </p>
                                                    <button type="submit" style='width: 100%;' class="btn btn-yellow">
                                                        <font color="black"> <i class="fa fa-plus"></i> <b>TAMBAHKAN CATATAN MEDIS </b></font>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                        
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover table-medis" id="#sample-table-2">
                                        <thead>
                                            <tr>
                                                <th width="1" class="center">NO</th>
                                                <th width="" class="center">TANGGAL BEROBAT</th>
                                                <th width="" class="col-to-export">CATATAN RIWAYAT MEDIS</th>
                                                <th width="1" class="col-to-export">ACT</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
            <!-- MODAL TAMBAH DATA -->
            <?php echo $modalTambahPasien; ?>
            <!-- END MODAL TAMBAH DATA -->
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['versi'] ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
    	<!-- ISI RIGHT SIDE BAR -->
    <!-- end: RIGHT SIDEBAR -->
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>
    
    
    <script src="<?php echo base_url();?>vendor/bower_components/twbs-pagination/jquery.twbsPagination.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.pulsate/jquery.pulsate.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-elements.min.js"></script>

    <!--BUTTON-->
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/spin.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-buttons.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <?php $this->load->view('admin/menu_bawah'); ?>
    <?php echo $this->session->flashdata('info2'); ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
           // TableExport.init();
        });
    </script>

    <script type="text/javascript">

        $(".table-pasien").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
              url: "<?php echo base_url('index.php/admin/data_pasien_optimized/'.$this->uri->segment(3).'') ?>",
              type:'POST',
            },
            columnDefs: [
                {
                    "targets": 4,
                    "orderable": false
                }
            ],
            order: [
                [0, "desc" ]
            ],
        });

        $(".table-medis").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
              url: "<?php echo base_url('index.php/admin/data_medis_optimized/'.$this->uri->segment(3).'') ?>",
              type:'POST',
            },
            columnDefs: [
                {
                    "targets": 1,
                    "orderable": false
                }
            ],
            order: [
                [0, "desc" ]
            ],
        });

    </script>

</body>

</html>