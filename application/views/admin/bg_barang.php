<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $data_config['nama_app'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>upload/icon/<?php echo $data_config['icon_app'] ?>" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />
    <?php $this->load->view('css') ?>

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row hidden-xs hidden-sm">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url() ?>index.php/admin?home=1">
                                    Home
                                </a>
                            </li>
                            <li class="active">
                                Data Produk
                            </li>
                        </ol>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
               <div class="row">
                <?php
                    if($status == 'Admin Klinik')
                    {
                ?>  

                    <div class="col-md-7">
                        <br>
                        <?php echo $this->session->flashdata('info'); ?>
                        <div class="alert alert-warning hidden-lg hidden-md hidden-xl">
                           <font color="black">
                              <h3>
                                 <b>Sisa masa server 
                                 <?php 
                                    $tgl1 = new DateTime(date('Y-m-d'));
                                    $tgl2 = new DateTime($data_config['end_hosting']);
                                    $jarak = $tgl2->diff($tgl1);

                                    echo $jarak->days;
                                 ?>
                                 hari lagi</b>
                              </h3>
                              Mulai server : <?php echo date('d F Y', strtotime($data_config['start_hosting'])) ?> <br>Berakhir server : <?php echo date('d F Y', strtotime($data_config['end_hosting'])) ?>
                           </font>

                        </div> 
                        <!--<div class="alert alert-warning hidden-xs hidden-sm">
                            <b>Warning !</b> - Silahkan gunakan <b>seacrh engine</b> pada sisi kanan tabel dibawah untuk pencarian lebih cepat
                        </div>-->
                        <div class="panel panel-default" style="background-color: ;">
                            <div class="panel-body">

                                <h4 align="center"><img width="30px" src="<?php echo base_url() ?>vendor/assets/images/icon/received.png"> <b> &nbsp;&nbsp;DATABASE BARANG KLINIK EDWCARE</b></h4>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12 space20">
                                        <a data-toggle="modal" style='background-color: #DC143C; width:100%' data-target="#modalTambahBarang" class="btn btn-red">
                                           <i class="fa fa-plus"></i> <b>TAMBAH BARANG </b>
                                        </a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover table-barang" id="#sample-table-2">
                                        <thead>
                                            <tr>
                                                <th width="140" class="col-to-export">#</th>
                                                <th width="140" class="col-to-export">BARANG</th>
                                                <th width="140" class="col-to-export">SISA STOK</th>
                                                <th width="140" class="col-to-export">HARGA JUAL SATUAN</th>
                                                <th width="120" class="">SATUAN</th>
                                                <th width="1" class="">KATEG</th>
                                                <th width="1" class="">ACTION</th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <br>
                                </div>
                            </div>
                        </div>

                    </div>
                    
                    <div class="col-md-5">
                        <br>
                        <?php echo $this->session->flashdata('info'); ?>
                       <!-- <div class="alert alert-warning">
                           <font color="black">
                              <h3>
                                 Sisa masa server 
                                 <?php 
                                    $tgl1 = new DateTime($data_config['start_hosting']);
                                    $tgl2 = new DateTime($data_config['end_hosting']);
                                    $jarak = $tgl2->diff($tgl1);

                                    echo $jarak->days;
                                 ?>
                                 hari lagi
                                 <br>
                                 
                              </h3>
                              Mulai server : <?php echo date('d F Y', strtotime($data_config['start_hosting'])) ?> <br>Berakhir server : <?php echo date('d F Y', strtotime($data_config['end_hosting'])) ?>
                           </font>

                        </div> -->
                        <!--<div class="alert alert-warning hidden-xs hidden-sm">
                            <b>Warning !</b> - Silahkan gunakan <b>seacrh engine</b> pada sisi kanan tabel dibawah untuk pencarian lebih cepat
                        </div>-->
                        <div class="panel panel-default">
                           <figure class="highcharts-figure">
                            <br>
                              <div id="container" style='width:100%; height:200px'></div>
                              <p class="highcharts-description">
                              </p>
                            </figure>
                        </div>

                    </div>
                    <div class="col-md-5">
                        <br>
                        <?php echo $this->session->flashdata('info'); ?>
                        <!--<div class="alert alert-warning hidden-xs hidden-sm">
                            <b>Warning !</b> - Silahkan gunakan <b>seacrh engine</b> pada sisi kanan tabel dibawah untuk pencarian lebih cepat
                        </div>-->
                        <div class="panel panel-default" style="background-color: ;">
                            <div class="panel-body">

                                <h4 align="center"><img width="30px" src="<?php echo base_url() ?>vendor/assets/images/icon/shopping-cart.png"> <b> &nbsp;&nbsp; PEMBELIAN STOK BARANG</b></h4>
                                <hr>
                                <form role="form" action="<?php echo base_url();?>index.php/admin/pembelianBarang" method="post">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <label>Opsi Barang dibeli:</label>
                                            <p>
                                                <select
                                                    id="select2insidemodal"
                                                    name="keluar_produk"
                                                    class="form-control"
                                                    required>
                                                    <option value="">
                                                        Pilih Produk
                                                    </option>
                                                    <?php
                                                        foreach($data_barang->result_array() as $d)
                                                        {
                                                            echo '<option value='.$d['barang_no'].'>['.$d['kategori_nama'].'] - 
                                                                    '.$d['barang_nama'].' - (Sisa '.$d['barang_stok'].' '.$d['barang_satuan'].')
                                                                </option>';
                                                        }
                                                    ?>
                                                </select>
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Qty:</label>
                                            <p>
                                                <input 
                                                    type="number" 
                                                    name="keluar_qty"
                                                    class="form-control"
                                                    placeholder="0" 
                                                    required>
                                            </p>
                                        </div>
                                        <div class="col-md-5">
                                            <label>Harga Beli:</label>
                                            <p>
                                                <input 
                                                    type="text" 
                                                    name="keluar_hg_beli"
                                                    class="form-control uang"
                                                    placeholder="20000" 
                                                    required>
                                            </p>
                                        </div>

                                        <div class="col-md-5">
                                            <label>Date:</label>
                                            <p>
                                                <input 
                                                    type="date" 
                                                    name="keluar_date"
                                                    class="form-control"
                                                    value="<?php echo date('Y-m-d') ?>"
                                                    required>
                                            </p>
                                        </div>
                                        <div class="col-md-8">
                                            <br>
                                            <button class="btn btn-yellow" type="submit" class="btn btn-red" 
                                            style="
                                                box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                padding: 10px; 
                                                border: 1px grey;
                                                width: 100%;">
                                                <b><font color="black">TAMBAH PEMBELIAN STOK</font></b>
                                            </button>
                                            
                                        </div>
                                        <div class="col-md-4">
                                            <br>
                                            <a style="
                                                box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                padding: 10px; 
                                                border: 1px grey;
                                                width: 100%;" class="btn btn-yellow" target="_blank" href="<?php echo base_url() ?>index.php/admin/bg_cetak_riwayat_pembelian"><img width="20px" src="<?php echo base_url() ?>vendor/assets/images/web/printer.png">  <font color="black"><b>PRINT OUT</b></font></a>
                                        </div>
                                    </div>
                                </form>
                                <br>
                                <br>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover table-pembelian" id="#sample-table-2">
                                        <thead>
                                            <tr>
                                                <th width="" class="">TANGGAL BELI</th>
                                                <th width="" class="col-to-export">BARANG DIBELI</th>
                                                <th width="1" class="col-to-export">QTY</th>
                                                <th width="" class="col-to-export">HARGA BELI</th>
                                                <th width="1" class=""></th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <br>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 space20">
                                        <p align="center"><font color="red"><b>PERHATIAN!!</b><br>Barang / jenis pengeluaran tidak terdaftar dalam opsi barang / opsi pengeluaran diatas? Silahkan tambah barang baru atau tambah pembelian / pengeluaran manual dengan klik pintasan dibawah!</font><br><br>
                                        <a data-toggle="modal" style='background-color: green; width:60%' data-target="#modalTambahPembelianManual" class="btn btn-green">
                                           <i class="fa fa-plus"></i> <b>CATAT PEMBELIAN MANUAL </b>
                                        </a></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                <?php } ?>
                <?php 
                    if($status == 'Staff Klinik')
                    {
                ?>
                    <div class="col-md-12">
                        <br>
                        <?php echo $this->session->flashdata('info'); ?>
                        <div class="alert alert-warning hidden-lg hidden-md hidden-xl">
                           <font color="black">
                              <h3>
                                 <b>Sisa masa server 
                                 <?php 
                                    $tgl1 = new DateTime(date('Y-m-d'));
                                    $tgl2 = new DateTime($data_config['end_hosting']);
                                    $jarak = $tgl2->diff($tgl1);

                                    echo $jarak->days;
                                 ?>
                                 hari lagi</b>
                              </h3>
                              Mulai server : <?php echo date('d F Y', strtotime($data_config['start_hosting'])) ?> <br>Berakhir server : <?php echo date('d F Y', strtotime($data_config['end_hosting'])) ?>
                           </font>

                        </div> 
                        <!--<div class="alert alert-warning hidden-xs hidden-sm">
                            <b>Warning !</b> - Silahkan gunakan <b>seacrh engine</b> pada sisi kanan tabel dibawah untuk pencarian lebih cepat
                        </div>-->
                        <div class="panel panel-default" style="background-color: ;">
                            <div class="panel-body">

                                <h4 align="center"><img width="30px" src="<?php echo base_url() ?>vendor/assets/images/icon/shopping-cart.png"> <b> &nbsp;&nbsp; PEMBELIAN STOK BARANG</b></h4>
                                <hr>
                                <form role="form" action="<?php echo base_url();?>index.php/admin/pembelianBarang" method="post">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <label>Opsi Barang dibeli:</label>
                                            <p>
                                                <select
                                                    id="select2insidemodal"
                                                    name="keluar_produk"
                                                    class="form-control"
                                                    required>
                                                    <option value="">
                                                        Pilih Produk
                                                    </option>
                                                    <?php
                                                        foreach($data_barang->result_array() as $d)
                                                        {
                                                            echo '<option value='.$d['barang_no'].'>['.$d['kategori_nama'].'] - 
                                                                    '.$d['barang_nama'].' - (Sisa '.$d['barang_stok'].' '.$d['barang_satuan'].')
                                                                </option>';
                                                        }
                                                    ?>
                                                </select>
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Qty:</label>
                                            <p>
                                                <input 
                                                    type="number" 
                                                    name="keluar_qty"
                                                    class="form-control"
                                                    placeholder="0" 
                                                    required>
                                            </p>
                                        </div>
                                        <div class="col-md-5">
                                            <label>Harga Beli:</label>
                                            <p>
                                                <input 
                                                    type="text" 
                                                    name="keluar_hg_beli"
                                                    class="form-control uang"
                                                    placeholder="20000" 
                                                    required>
                                            </p>
                                        </div>

                                        <div class="col-md-5">
                                            <label>Date:</label>
                                            <p>
                                                <input 
                                                    type="date" 
                                                    name="keluar_date"
                                                    class="form-control"
                                                    value="<?php echo date('Y-m-d') ?>"
                                                    required>
                                            </p>
                                        </div>
                                        <div class="col-md-8">
                                            <br>
                                            <button class="btn btn-yellow" type="submit" class="btn btn-red" 
                                            style="
                                                box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                padding: 10px; 
                                                border: 1px grey;
                                                width: 100%;">
                                                <b><font color="black">CATAT PEMBELIAN STOK</font></b>
                                            </button>
                                            
                                        </div>
                                        <div class="col-md-4">
                                            <br>
                                            <a style="
                                                box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                padding: 10px; 
                                                border: 1px grey;
                                                width: 100%;" data-toggle="modal" style='background-color: green; width:60%' data-target="#modalTambahPembelianManual" class="btn btn-green">
                                           <i class="fa fa-plus"></i> <b>CATAT PENGELUARAN HARIAN </b>
                                        </a>
                                        </div>
                                    </div>
                                </form>
                                <br>
                                <br>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover table-pembelian" id="#sample-table-2">
                                        <thead>
                                            <tr>
                                                <th width="" class="">TANGGAL BELI</th>
                                                <th width="" class="col-to-export">BARANG DIBELI</th>
                                                <th width="1" class="col-to-export">QTY</th>
                                                <th width="" class="col-to-export">HARGA BELI</th>
                                                <th width="1" class=""></th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <br>
                                </div>
                                
                            </div>
                        </div>

                    </div>
            <?php } ?>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
            <!-- MODAL TAMBAH DATA -->
            <?php echo $modalTambahPembelianManual; ?>
            <?php echo $modalTambahBarang; ?>
            <?php echo $modalEditBarang; ?>
            <!-- END MODAL TAMBAH DATA -->
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['versi'] ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
    	<!-- ISI RIGHT SIDE BAR -->
    <!-- end: RIGHT SIDEBAR -->
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>
    
    
    <script src="<?php echo base_url();?>vendor/bower_components/twbs-pagination/jquery.twbsPagination.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.pulsate/jquery.pulsate.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-elements.min.js"></script>

    <!--BUTTON-->
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/spin.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/jquery.mask.js"></script>


    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/item-series.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <?php $this->load->view('admin/menu_bawah'); ?>
    <?php echo $this->session->flashdata('info2'); ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
           // TableExport.init();
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){

            // Format mata uang.
            $( '.uang' ).mask('000.000.000', {reverse: true});

        })
    </script>
<!--
    <script type="text/javascript">

        $(".table-barang").DataTable({
            ordering: false,
            processing: true,
            serverSide: true,
            ajax: {
              url: "<?php echo base_url('index.php/admin/data_barang_optimized') ?>",
              type:'POST',
            }
        });

    </script>
-->

    <script type="text/javascript">

        $(".table-barang").DataTable({
            /*dom: 'Bfrtip',
            buttons: [
                'print'
            ],*/
            
            processing: true,
            serverSide: true,

            ajax: {
              url: "<?php echo base_url('index.php/admin/data_barang_optimized') ?>",
              type:'POST',
            },
            columnDefs: [
                {
                    "targets": 6,
                    "orderable": false
                }
            ],

            order: [
                [0, "desc" ]
            ],

        });

        $(".table-pembelian").DataTable({
          /*  dom: 'Bfrtip',
                buttons: [
                    'print'
                ], */
            processing: true,
            serverSide: true,
            ajax: {
              url: "<?php echo base_url('index.php/admin/data_pembelian_optimized') ?>",
              type:'POST',
            },
            columnDefs: [
                {
                    "targets": 4,
                    "orderable": false
                }
            ],
            order: [
                [0, "desc" ]
            ],
        });

    </script>



    <script type="text/javascript">
        Highcharts.chart('container', {

  chart: {
    type: 'item'
  },

  title: {
    text: '<b>TOTAL PRODUK / KATEGORI</b>'
  },

  subtitle: {
    text: 'Akumulasi seluruh produk terdaftar'
  },

  legend: {
    labelFormat: '{name} <span style="opacity: 0.4">{y}</span>'
  },

  series: [{
    name: 'Jumlah Item',
    keys: ['name', 'y', 'color', 'label'],
    data: [
    <?php 
        foreach($grafik_kategori->result_array() as $d)
        {
            if($d['kategori_no'] == 2)
            {
                echo "['".$d['kategori_nama']."', ".$d['total'].", '#DC143C', '".$d['kategori_nama']."'],";
            }
            else if($d['kategori_no'] == 3)
            {
                echo "['".$d['kategori_nama']."', ".$d['total'].", 'green', '".$d['kategori_nama']."'],";
            }
            else
            {
                echo "['".$d['kategori_nama']."', ".$d['total'].", 'orange', '".$d['kategori_nama']."'],";
            }
            
        }
    ?>
      
    ],
    dataLabels: {
      enabled: true,
      format: '{point.label}'
    },

    // Circular options
    center: ['50%', '88%'],
    size: '170%',
    startAngle: -100,
    endAngle: 100
  }],

  responsive: {
    rules: [{
      condition: {
        maxWidth: 600
      },
      chartOptions: {
        series: [{
          dataLabels: {
            distance: -30
          }
        }]
      }
    }]
  }
});
    </script>

     <script type="text/javascript">
      $(document).ready(function() {
          $("#select2insidemodal").select2({
            width: '100%', 
          });
        });
    </script>



</body>

</html>