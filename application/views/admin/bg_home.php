<?php
    function rupiah($angka){
    
    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
    return $hasil_rupiah;
 
}
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $data_config['nama_app'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>upload/icon/<?php echo $data_config['icon_app'] ?>" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="<?php echo $data_config['nama_app'] ?>" name="description" />
    <meta content="<?php echo $data_config['nama_app'] ?>" name="multidatanesia" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />
    <?php $this->load->view('css') ?>

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url(); ?>vendor/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/jquery.tagsinput/dist/jquery.tagsinput.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/summernote/dist/summernote.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    <!-- DATA GRAFIK -->
    <style type="text/css">
        @import 'https://code.highcharts.com/css/highcharts.css';
    </style>

    <!-- DATA GRAFIK -->
    <style type="text/css">
        @import 'https://code.highcharts.com/css/highcharts.css';

        .highcharts-figure,
        .highcharts-data-table table {
          min-width: 100%;
          max-width: 100%;
          margin: 1em auto;
        }

        .highcharts-data-table table {
          font-family: Verdana, sans;
          border-collapse: collapse;
          border: 1px solid #ebebeb;
          margin: 10px auto;
          text-align: center;
          width: 100%;
          max-width: 500px;
        }

        .highcharts-data-table caption {
          padding: 1em 0;
          font-size: 1.2em;
          color: #555;
        }

        .highcharts-data-table th {
          font-weight: 600;
          padding: 0.5em;
        }

        .highcharts-data-table td,
        .highcharts-data-table th,
        .highcharts-data-table caption {
          padding: 0.5em;
        }

        .highcharts-data-table thead tr,
        .highcharts-data-table tr:nth-child(even) {
          background: #f8f8f8;
        }

        .highcharts-data-table tr:hover {
          background: #f1f7ff;
        }
    </style>


</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row hidden-xs hidden-sm">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li class="active">
                                    Home
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <?php echo $this->session->flashdata('info'); ?>
                <div class="row">
                    
                    <div class="col-md-12">
                        <?php echo $this->session->flashdata('info'); ?>
                        <!--<div class="alert alert-warning hidden-xs hidden-sm">
                            <b>Warning !</b> - Silahkan gunakan <b>seacrh engine</b> pada sisi kanan tabel dibawah untuk pencarian lebih cepat
                        </div>-->
                        <br>
                        <div class="alert alert-warning hidden-lg hidden-md hidden-xl">
                           <font color="black">
                              <h3>
                                 <b>Sisa masa server 
                                 <?php 
                                    $tgl1 = new DateTime(date('Y-m-d'));
                                    $tgl2 = new DateTime($data_config['end_hosting']);
                                    $jarak = $tgl2->diff($tgl1);

                                    echo $jarak->days;
                                 ?>
                                 hari lagi</b>
                              </h3>
                              Mulai server : <?php echo date('d F Y', strtotime($data_config['start_hosting'])) ?> <br>Berakhir server : <?php echo date('d F Y', strtotime($data_config['end_hosting'])) ?>
                           </font>

                        </div> 
                         <div class="panel panel-default col-md-12" style="background-color: #F0FFF0;">
                            <div class="panel-body">
                                <div class="row">
                                    <form role="form" action="<?php echo base_url();?>index.php/admin/addtocart" method="post">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12">
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <a style="
                                                                box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                padding: 10px; 
                                                                border: 10px dash grey;
                                                                background-color: white;" 
                                                                class="btn btn-icon btn-block">
                                                                <h3><b><?php echo rupiah($total_pemasukan['sum']) ?></b></h3>
                                                                <span class="badge badge-yellow" 
                                                                style="
                                                                    box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                    padding: 10px; 
                                                                    border: 1px grey;" >
                                                                <font color="black"><b>PEMASUKAN</b> KESELURUHAN</font></span>
                                                            </a>
                                                            <p align="center"><font>Total pemasukan keseluruhan</font></p>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <a style="
                                                                box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                padding: 10px; 
                                                                background-color: white;
                                                                border: 10px dash grey;" 
                                                                class="btn btn-icon btn-block">
                                                                <h3><b><?php echo rupiah($pemasukan_bln_ini['sum']) ?></b></h3>
                                                                <span class="badge badge-danger"
                                                                style="
                                                                    box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                    padding: 10px; 
                                                                    border: 1px grey;">
                                                                PEMASUKAN <b>BULAN BERJALAN</b></span>
                                                            </a>
                                                            <p align="center"><font>Total pemasukan <?php echo date('F Y'); ?></font></p>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <a style="
                                                                box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                padding: 10px; 
                                                                background-color: white;
                                                                border: 10px dash grey;" 
                                                                class="btn btn-icon btn-block">
                                                                <h3><b><?php echo rupiah($pemasukan_hr_ini['sum']) ?></b></h3>
                                                                <span class="badge badge-green"
                                                                style="
                                                                    box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                    padding: 10px; 
                                                                    border: 1px grey;">
                                                                PEMASUKAN <b>HARI INI</b></span>
                                                            </a>
                                                            <p align="center"><font>Total pemasukan <?php echo date('d F Y') ?></font></p>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <a style="
                                                                box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                padding: 10px; 
                                                                background-color: white;
                                                                border: 10px dash grey;" 
                                                                class="btn btn-icon btn-block">
                                                                <h3><b><?php echo $transaksi_hr_ini['count'] ?> Transaksi Masuk</b></h3>
                                                                <span class="badge badge-danger"
                                                                style="
                                                                    box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                    padding: 10px; 
                                                                    border: 1px grey;">
                                                                TRANSAKSI MASUK <b>HARI INI</b></span>
                                                            </a>
                                                            <p align="center"><font>Total transaksi masuk <?php echo date('d F Y') ?></font></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="row">
                                    <form role="form" action="<?php echo base_url();?>index.php/admin/addtocart" method="post">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12">
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <a style="
                                                                box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                padding: 10px; 
                                                                border: 10px dash grey;
                                                                background-color: white;" 
                                                                class="btn btn-icon btn-block">
                                                                <h3><b><?php echo rupiah($total_pengeluaran['sum']) ?></b></h3>
                                                                <span class="badge badge-yellow" 
                                                                style="
                                                                    box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                    padding: 10px; 
                                                                    border: 1px grey;" >
                                                                <font color="black"><b>PENGELUARAN</b> KESELURUHAN</font></span>
                                                            </a>
                                                            <p align="center"><font>Total pegeluaran keseluruhan</font></p>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <a style="
                                                                box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                padding: 10px; 
                                                                background-color: white;
                                                                border: 10px dash grey;" 
                                                                class="btn btn-icon btn-block">
                                                                <h3><b><?php echo rupiah($pengeluaran_bln_ini['sum']) ?></b></h3>
                                                                <span class="badge badge-danger"
                                                                style="
                                                                    box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                    padding: 10px; 
                                                                    border: 1px grey;">
                                                                PENGELUARAN <b>BULAN BERJALAN</b></span>
                                                            </a>
                                                            <p align="center"><font>Total pengeluaran <?php echo date('F Y'); ?></font></p>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <a style="
                                                                box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                padding: 10px; 
                                                                background-color: white;
                                                                border: 10px dash grey;" 
                                                                class="btn btn-icon btn-block">
                                                                <h3><b><?php echo rupiah($pengeluaran_hr_ini['sum']) ?></b></h3>
                                                                <span class="badge badge-green"
                                                                style="
                                                                    box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                    padding: 10px; 
                                                                    border: 1px grey;">
                                                                PENGELUARAN <b>HARI INI</b></span>
                                                            </a>
                                                            <p align="center"><font>Total pengeluaran <?php echo date('d F Y') ?></font></p>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <a style="
                                                                box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                padding: 10px; 
                                                                background-color: white;
                                                                border: 10px dash grey;" 
                                                                class="btn btn-icon btn-block">
                                                                <h3><b><?php echo $transaksi_keluar_hr_ini['count'] ?> Transaksi Keluar</b></h3>
                                                                <span class="badge badge-danger"
                                                                style="
                                                                    box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                                    padding: 10px; 
                                                                    border: 1px grey;">
                                                                TRANSAKSI KELUAR <b>HARI INI</b></span>
                                                            </a>
                                                            <p align="center"><font>Total transaksi keluar <?php echo date('d F Y') ?></font></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="panel panel-default">
                            
                            <div class="panel-body">
                                <figure class="highcharts-figure">
                                  <div id="pemasukan"></div>
                                  
                                </figure>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="panel panel-default">
                            
                            <div class="panel-body">
                                <figure class="highcharts-figure">
                                  <div id="pemasukan2"></div>
                                  
                                </figure>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="panel panel-default">
                            
                            <div class="panel-body">
                                <figure class="highcharts-figure">
                                  <div id="pengeluaran"></div>
                                  
                                </figure>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->

    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['versi'] ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="https://code.highcharts.com/highcharts.src.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-maxlength/src/bootstrap-maxlength.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/autosize/dist/autosize.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery-maskmoney/dist/jquery.maskMoney.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/summernote/dist/summernote.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ckeditor/adapters/jquery.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-fileinput/js/fileinput.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/form-elements.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>

    <?php $this->load->view('admin/menu_bawah'); ?>
    <?php echo $this->session->flashdata('info2'); ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
        });
    </script>

    <script type="text/javascript">
        Highcharts.chart('pemasukan', {

           

          title: {
            text: 'MONITORING TOTAL PEMASUKAN <b>HARIAN</b> BY KASIR I & II <b>(PADA BULAN BERJALAN)</b>'
          },

          subtitle: {
            text: 'Sumber: Data bulan <b><?php echo date('F-Y') ?></b>'
          },

          yAxis: {
            title: {
              text: 'Nominal Transaksi'
            }
          },

          xAxis: {
            accessibility: {
              rangeDescription: 'Transaksi'
            }
          },

          legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
          },

          plotOptions: {
            series: {
              label: {
                connectorAllowed: false
              },
              pointStart: 1
            }
          },

          series: [{
            name: 'Pemasukan Harian (Rp)',
            color: 'green',
            data: [
            <?php
              foreach($data_transaksi->result_array () as $d)
              {
                echo "['".date('d-F-Y H:i', strtotime($d['transaksi_date']))." WIB',".$d['sum']."],";
              }
            ?>
        ]
          }],

          responsive: {
            rules: [{
              condition: {
                maxWidth: 500
              },
              chartOptions: {
                legend: {
                  layout: 'horizontal',
                  align: 'center',
                  verticalAlign: 'bottom'
                }
              }
            }]
          }

        });
    </script>

    <script type="text/javascript">
        Highcharts.chart('pengeluaran', {

           

          title: {
            text: 'MONITORING <b>PENGELUARAN HARIAN</b> (BULAN BERJALAN)'
          },

          subtitle: {
            text: 'Sumber: Data bulan <b><?php echo date('F-Y') ?></b>'
          },

          yAxis: {
            title: {
              text: 'Nominal Transaksi'
            }
          },

          xAxis: {
            accessibility: {
              rangeDescription: 'Transaksi'
            }
          },

          legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
          },

          plotOptions: {
            series: {
              label: {
                connectorAllowed: false
              },
              pointStart: 1
            }
          },

          series: [
          {
            name: 'Pengeluaran Harian (Rp)',
            color: 'red',
            data: [
            <?php
              foreach($data_pengeluaran->result_array () as $d)
              {
                echo "['".date('d-F-Y H:i', strtotime($d['keluar_date']))." WIB',".$d['sum']."],";
              }
            ?>
        ]
          }],

          responsive: {
            rules: [{
              condition: {
                maxWidth: 500
              },
              chartOptions: {
                legend: {
                  layout: 'horizontal',
                  align: 'center',
                  verticalAlign: 'bottom'
                }
              }
            }]
          }

        });
    </script>

     <script type="text/javascript">
        Highcharts.chart('pemasukan2', {

           

          title: {
            text: 'MONITORING TOTAL PEMASUKAN <b>PER TRANSAKSI</b> BY KASIR I & II <b>(PADA HARI INI)</b>'
          },

          subtitle: {
            text: 'Sumber: Data Tanggal <b><?php echo date('d-F-Y') ?></b>'
          },

          yAxis: {
            title: {
              text: 'Nominal Transaksi'
            }
          },

          xAxis: {
            accessibility: {
              rangeDescription: 'Transaksi'
            }
          },

          legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
          },

          plotOptions: {
            series: {
              label: {
                connectorAllowed: false
              },
              pointStart: 1
            }
          },

          series: [
          {
            name: 'Pemasukan Per Transaksi (Rp)',
            color: 'orange',
            data: [
            <?php
              foreach($data_per_transaksi->result_array () as $d)
              {
                echo "['".date('d-F-Y H:i', strtotime($d['transaksi_date']))." WIB',".$d['transaksi_total']."],";
              }
            ?>
        ]
          }],

          responsive: {
            rules: [{
              condition: {
                maxWidth: 500
              },
              chartOptions: {
                legend: {
                  layout: 'horizontal',
                  align: 'center',
                  verticalAlign: 'bottom'
                }
              }
            }]
          }

        });
    </script>

</body>

</html>