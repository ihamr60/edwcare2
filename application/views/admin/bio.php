<style type="text/css">
   .upper   { text-transform: uppercase; }
</style>

<div class="alert alert-info hidden-xs hidden-sm">
   <button data-dismiss="alert" class="close">
       &times;
   </button>
   <font color="black">
      <?php

      date_default_timezone_set("Asia/Jakarta");

      $b = time();
      $jam = date("G",$b);

      if ($jam>=0 && $jam<=11)
      {
      echo "Selamat Pagi ";
      }
      else if ($jam >=12 && $jam<=14)
      {
      echo "Selamat Siang";
      }
      else if ($jam >=15 && $jam<=17)
      {
      echo "Selamat Sore";
      }
      else if ($jam >=17 && $jam<=18)
      {
      echo "Selamat Petang";
      }
      else if ($jam >=19 && $jam<=23)
      {
      echo "Selamat Malam";
      }

      ?> 
      <strong> 
         <?php echo $nama; ?>   
      </strong>. 
      Anda login akses sebagai 
      <strong class="#"><?php echo $status;?></strong>

      </font>

</div> 
<!--<div class="alert alert-warning">
   <font color="black">
      <h3>
         Sisa masa server 
         <?php 
            $tgl1 = new DateTime($data_config['start_hosting']);
            $tgl2 = new DateTime($data_config['end_hosting']);
            $jarak = $tgl2->diff($tgl1);

            echo $jarak->days;
         ?>
         hari lagi
         <br>
         
      </h3>
      Mulai tanggal : <?php echo date('d F Y', strtotime($data_config['start_hosting'])) ?> <br>Berakhir tanggal : <?php echo date('d F Y', strtotime($data_config['end_hosting'])) ?>
   </font>

</div> -->



