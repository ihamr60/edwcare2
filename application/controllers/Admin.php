<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	/**
	 * Created by Ilham Ramadhan, S.Tr.Kom
	 * IT PDAM Kota langsa
	 * HP : 0853 6188 5100
	 * Email: ilhamr6000@gmail.com
	 */

	private $db2;

	 public function __construct()
	 {
	  parent::__construct();
	         $this->db2 = $this->load->database('db2', TRUE);
	 }

	public function test_print()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Klinik')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada



			
			$bc['data_barang']		= $this->web_app_model->getAllData('tbl_barang');
			$bc['total_belanja']	= $this->web_app_model->getSUMAllData('tbl_cart','cart_jumlah','cart_paid','0');
			$bc['total_produk']		= $this->web_app_model->getCOUNTAllData('tbl_cart','cart_jumlah','cart_paid','0');
			//$bc['data_cart']		= $this->web_app_model->getAll2Join('cart_item','barang_no','tbl_cart','tbl_barang');

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['atas'] 				= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('admin/bio',$bc,true);	
			$bc['modalBayar'] 			= $this->load->view('admin/modalBayar',$bc,true);	
			$this->load->view('test_print',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function index()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Klinik')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['data_pengeluaran']		= $this->web_app_model->pengeluaran_group_date();
			$bc['data_transaksi']		= $this->web_app_model->transaksi_group_date();
			$bc['data_per_transaksi']	= $this->web_app_model->transaksi_per_transaksi();
			$bc['total_pemasukan']		= $this->web_app_model->getSUMAllData('tbl_transaksi','transaksi_total','transaksi_stts','1');
			$bc['pemasukan_bln_ini']	= $this->web_app_model->getSUMAllData2('tbl_transaksi','transaksi_total','MONTH(transaksi_date)',date('m'), 'YEAR(transaksi_date)',date('Y'));
			$bc['pemasukan_hr_ini']		= $this->web_app_model->getSUMAllData3('tbl_transaksi','transaksi_total','MONTH(transaksi_date)',date('m'), 'YEAR(transaksi_date)',date('Y'), 'DAY(transaksi_date)',date('d'));

			//-=======
			$bc['total_pengeluaran']	= $this->web_app_model->getSUMAllData_nowhere('tbl_pengeluaran','keluar_hg_beli');
			$bc['pengeluaran_bln_ini']	= $this->web_app_model->getSUMAllData2('tbl_pengeluaran','keluar_hg_beli','MONTH(keluar_date)',date('m'), 'YEAR(keluar_date)',date('Y'));
			$bc['pengeluaran_hr_ini']	= $this->web_app_model->getSUMAllData3('tbl_pengeluaran','keluar_hg_beli','MONTH(keluar_date)',date('m'), 'YEAR(keluar_date)',date('Y'), 'DAY(keluar_date)',date('d'));

			$bc['transaksi_keluar_hr_ini']	= $this->web_app_model->getCOUNTAllData3('tbl_pengeluaran','keluar_no','MONTH(keluar_date)',date('m'), 'YEAR(keluar_date)',date('Y'), 'DAY(keluar_date)',date('d'));


			$bc['transaksi_hr_ini']	= $this->web_app_model->getCOUNTAllData3('tbl_transaksi','transaksi_no','MONTH(transaksi_date)',date('m'), 'YEAR(transaksi_date)',date('Y'), 'DAY(transaksi_date)',date('d'));

			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	
			$this->load->view('admin/bg_home',$bc);
		}
		else if(!empty($cek) && $stts=='Staff Klinik')
		{
			header('location:'.base_url().'index.php/admin/bg_transaksi?transaksi=1');
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

// BAGIAN TAGIHAN SERVER START

	public function bg_tagihan_server()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Klinik' || $stts=='Staff Klinik')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['atas'] 				= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('admin/bio',$bc,true);	
			//$bc['modalBayar'] 			= $this->load->view('admin/modalBayar',$bc,true);	
			$this->load->view('admin/bg_tagihan_server',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function bg_upload_pembayaran()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Klinik' || $stts=='Staff Klinik')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['data_invoice']		= $this->web_app_model->getWhereOneItem_db2($this->uri->segment(3),'inv_kdinvoice','tbl_invoice');

			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['atas'] 				= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('admin/bio',$bc,true);	
			//$bc['modalBayar'] 			= $this->load->view('admin/modalBayar',$bc,true);	
			$this->load->view('admin/bg_upload_pembayaran',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function upload_pembayaran()
	{
		$inv_kdinvoice				= $this->input->post('inv_kdinvoice');

		// get foto
		 $config['upload_path'] 	= './upload/kwitansi_invoice';
		 $config['allowed_types'] 	= 'jpg|png|jpeg|pdf';
		 $config['max_size'] 		= '10000';  //10MB max
		 //$config['max_width'] 	= '4480'; // pixel
		 //$config['max_height'] 	= '4480'; // pixel
		 $config['overwrite']		= true;
	     $config['file_name'] 		= "KWITANSI-".$inv_kdinvoice;

     	 $this->load->library('upload', $config);


			if(!empty($_FILES['inv_kwitansi']['name'])) 
			{
		        if ( $this->upload->do_upload('inv_kwitansi') ) {
		            $foto = $this->upload->data();		

					$data = array(		
						'inv_stts' 			=> "2",
						'inv_kwitansi' 		=> $foto['file_name'],
						);

					$where = array(
						'inv_kdinvoice'		=> $inv_kdinvoice,
					);

		
					$this->web_app_model->updateDataWhere_db2($where, $data,'tbl_invoice');
					header('location:'.base_url().'index.php/admin/bg_tagihan_server?server=1/');
					$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
															<button type='button' class='close' data-dismiss='alert'>
																<i class='icon-remove'></i>
															</button>
					
															<p>
															<font color='black'>
																<strong>
																	<i class='icon-ok'></i>
																	Success! - 
																</strong>
																Invoice berhasil diupdate!
															</font>
															</p>
														</div>");

					$this->session->set_flashdata("info2","<script type='text/javascript'>
														     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Invoice berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
				}
				else 
				{
		          	header('location:'.base_url().'index.php/admin/bg_tagihan_server?server=1');
					$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Maaf!',
										                text:  'Data gagal tersimpan, pastikan File max 10 Mb atau hubungi Konsultan IT',
										                type: 'warning',
										                timer: 300000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>");
		    	}
		    }
		    else 
			{
	          	header('location:'.base_url().'index.php/admin/bg_tagihan_server?server=1/');
				$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'File kosong!',
									                text:  'Mohon lampirkan file Invoice',
									                type: 'warning',
									                timer: 300000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>");
		    }
	}

	function data_invoice(){

        $draw 		= $this->input->post('draw');
       	

        if($this->input->post('search')['value'] == '')
        {
        	$search 	= $this->uri->segment(3);
        }
        else
        {
        	$search 	= $this->input->post('search')['value'];
        }

        
        $start 		= $this->input->post('start');
        $length 	= $this->input->post('length');
        $order 		= $this->input->post('order');

		$this->db2->from('tbl_invoice')
				->where('inv_client','EWC');

		if($order){
			foreach ($order as $value) {
				switch ($value['column']) {
					case 0:
						$this->db2->order_by('inv_no', $value['dir']);
						break;
					case 1:
						$this->db2->order_by('inv_kdinvoice', $value['dir']);
						break;
					case 2:
						$this->db2->order_by('inv_client', $value['dir']);
						break;
					case 3:
						$this->db2->order_by('inv_periode', $value['dir']);
						break;
					case 4:
						$this->db2->order_by('inv_nominal', $value['dir']);
						break;
					case 5:
						$this->db2->order_by('inv_surat', $value['dir']);
						break;
					case 6:
						$this->db2->order_by('inv_stts', $value['dir']);
						break;
					case 7:
						$this->db2->order_by('inv_kwitansi', $value['dir']);
						break;
				}
			}
		}
			
		if($search!=""){
			$this->db2->like("inv_kdinvoice",$search);
			$this->db2->or_like("inv_client",$search);
		}

		$tempdb = clone $this->db2;
		$output['recordsFiltered'] = $tempdb->count_all_results();

		$this->db2->limit($length,$start);

		$data = $this->db2->get()->result_array();

		function rupiah($angka){
		    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
		    return $hasil_rupiah;
    	}

    	$no=1;
		foreach ($data as $d) {
		
			if($d['inv_stts'] == 1)
			{
				$stts 			= "<span style='text-transform: uppercase;' class='label label-success'>LUNAS</span>";
				$action 		= "LUNAS";
			}
			else if($d['inv_stts'] == 0)
			{
				$stts = "<span style='text-transform: uppercase;' class='label label-danger'>BELUM LUNAS</span>";
				$action = "<div class='btn-group'>
                    <a class='btn btn-green dropdown-toggle btn-xs' data-toggle='dropdown' href='#'>
                        <i style='color: white;' class='fa fa-cog'></i> <span class='caret'></span>
                    </a>
                    <ul role='menu' class='dropdown-menu pull-right'>
                        <li role='presentation'>
                            <a role='menuitem' tabindex='-1' href='".base_url()."index.php/admin/bg_upload_pembayaran/".$d['inv_kdinvoice']."?server=1'>
                                <i class='fa fa-send-o'></i> Upload Bukti Pembayaran
                            </a>
                        </li>
                    </ul>
                </div>";
			}
			else if($d['inv_stts'] == 2)
			{
				$stts = "<span style='text-transform: uppercase;' class='label label-yellow'><font color='black'>MENUNGGU VERIFIKASI</font></span>";
				$action = "<div class='btn-group'>
                    <a class='btn btn-green dropdown-toggle btn-xs' data-toggle='dropdown' href='#'>
                        <i style='color: white;' class='fa fa-cog'></i> <span class='caret'></span>
                    </a>
                    <ul role='menu' class='dropdown-menu pull-right'>
                        <li role='presentation'>
                            <a role='menuitem' tabindex='-1' href='".base_url()."index.php/admin/bg_upload_pembayaran/".$d['inv_kdinvoice']."?server=1'>
                               <i class='fa fa-send-o'></i> Upload Bukti Pembayaran
                            </a>
                        </li>
                    </ul>
                </div>";
			}

			if($d['inv_kwitansi'] != "")
			{
				$kwitansi 		= "<a href='https://edwcare.acehdev.web.id/upload/kwitansi_invoice/".$d['inv_kwitansi']."'><font color='green'>".$d['inv_kwitansi']."</font></a>";
			}
			else if($d['inv_kwitansi'] == "")
			{
				$kwitansi 		= "Tidak tersedia";
			}

			$hapus 					= '"Are you sure you want to delete the '.$d['inv_kdinvoice'].'?"';

			$output['data'][]=array(
				$no,
				$d['inv_kdinvoice'],
				$d['inv_client'],
				$d['inv_periode'],
				rupiah($d['inv_nominal']),
				"<a href='https://acehdev.web.id/hosting/upload/invoice/".$d['inv_surat']."'><font color='green'>".$d['inv_surat']."</font></a>",
				$stts,
				$kwitansi,
				$action);
			$no++;
		}

		$output['recordsTotal'] = $this->db2->from('tbl_invoice')
											->where('inv_client','EWC')
											->get()->num_rows();

		echo json_encode($output);
    } 

// BAGIAN TAGIHAN SERVER END

// BAGIAN GANTI PASSWORD - START

	public function bg_password()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Klinik' || $stts=='Staff Klinik')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['atas'] 				= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('admin/bio',$bc,true);	
			//$bc['modalBayar'] 			= $this->load->view('admin/modalBayar',$bc,true);	
			$this->load->view('admin/bg_password',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function gantiPassword()
	{		
		$user_username		= $this->input->post('user_username');
		$user_pwd_baru_1	= $this->input->post('user_pwd_baru_1');
		$user_pwd_baru_2	= $this->input->post('user_pwd_baru_2');

		if($user_pwd_baru_1 == $user_pwd_baru_2)
		{
			$data = array(		
			'user_pwd' 				=> md5($user_pwd_baru_1),
			);

			$where = array(		
				'user_username' 	=> $user_username,
				);
			
			$this->web_app_model->updateDataWhere($where,$data,'tbl_user');
			header('location:'.base_url().'index.php/admin/bg_password?pass=1');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data password berhasil diupdate !
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data password berhasil diupdate !',
											                type: 'success',
											                timer: 50000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
		}
		else
		{
			header('location:'.base_url().'index.php/admin/bg_password?pass=1');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-warning'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														Password tidak sama! Coba lagi
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
											     swal({
											                title: 'Gagal!!',
											                text:  'Password tidak sama! Coba lagi',
											                type: 'warning',
											                timer: 50000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
		}
		
	}

// BAGIAN GANTI PASSWORD - END

// BAGIAN TRANSAKSI -- START

	public function bg_transaksi()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Klinik' || $stts=='Staff Klinik')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada



			$bc['data_barang']		= $this->web_app_model->getAll2Join('barang_kategori','kategori_no','tbl_barang','tbl_kategori');
			$bc['total_belanja']	= $this->web_app_model->getSUMAllData('tbl_cart','cart_jumlah','cart_paid','0');
			$bc['total_produk']		= $this->web_app_model->getCOUNTAllData('tbl_cart','cart_jumlah','cart_paid','0');
			//$bc['data_cart']		= $this->web_app_model->getAll2Join('cart_item','barang_no','tbl_cart','tbl_barang');

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['atas'] 				= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('admin/bio',$bc,true);	
			$bc['modalBayar'] 			= $this->load->view('admin/modalBayar',$bc,true);	
			$this->load->view('admin/bg_transaksi',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function cetak_struk()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Klinik' || $stts=='Staff Klinik')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada



			$bc['data_cart']		= $this->web_app_model->getWhereAll2Join_left('cart_item','barang_no','tbl_cart','tbl_barang','cart_id',$this->uri->segment(3));
			$bc['total_belanja']	= $this->web_app_model->getWhereOneItem($this->uri->segment(3),'transaksi_cart_id','tbl_transaksi');
			//$bc['total_produk']		= $this->web_app_model->getCOUNTAllData('tbl_cart','cart_jumlah','cart_paid','0');
			//$bc['data_cart']		= $this->web_app_model->getAll2Join('cart_item','barang_no','tbl_cart','tbl_barang');

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['atas'] 				= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('admin/bio',$bc,true);	
			//$bc['modalBayar'] 			= $this->load->view('admin/modalBayar',$bc,true);	
			$this->load->view('admin/struk',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function hapusCart()
	{
		$cart_no		= $this->uri->segment(3);
		$hapus 			= array('cart_no'=>$cart_no);

		$cek_cart 		= $this->web_app_model->get2WhereOneItem($cart_no,'cart_no','1','cart_kasir','tbl_cart');

		if(!empty($cek_cart))
		{
			$this->web_app_model->penambahan_stok($cek_cart['cart_qty'],$cek_cart['cart_item']);
		}

		$this->web_app_model->deleteData('tbl_cart',$hapus);
		header('location:'.base_url().'index.php/admin/bg_transaksi?transaksi=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Item has been deleted!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Item has been deleted!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function addtocart()
	{
		$cart_item				= $this->input->post('cart_item');
		$cart_qty				= $this->input->post('cart_qty');

		$cek_harga              = $this->web_app_model->getWhereOneItem($cart_item,'barang_no','tbl_barang');

		$data = array(		
			'cart_item' 		=> $cart_item,
			'cart_qty' 			=> $cart_qty,
			'cart_harga' 		=> $cek_harga['barang_harga'],
			'cart_jumlah' 		=> $cek_harga['barang_harga']*$cart_qty,
			'cart_kasir' 		=> '1',
			);

		$cek_stok  				= $cek_harga['barang_stok'] - $cart_qty;

		if($cek_stok >= 0)
		{
			$this->web_app_model->insertData($data,'tbl_cart');
			$this->web_app_model->pengurangan_stok($cart_qty,$cart_item);

			header('location:'.base_url().'index.php/admin/bg_transaksi?transaksi=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Produk berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Produk berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
		}
		else
		{
			header('location:'.base_url().'index.php/admin/bg_transaksi?transaksi=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-danger'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-remove'></i>
															Gagal! - 
														</strong>
														Stok tidak mencukupi!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
											     swal({
											                title: 'Failed !',
											                text:  'Stok tidak cukup!',
											                type: 'warning',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
		}
		
		
		
	}

	public function addtocart_2()
	{
		$cart_item				= $this->input->post('cart_item');
		$cart_qty				= '1';
		$cart_harga				= str_replace(".", "", $this->input->post('cart_harga'));
		$cart_jumlah			= str_replace(".", "", $this->input->post('cart_harga'));

	//	$cek_harga              = $this->web_app_model->getWhereOneItem($cart_item,'barang_no','tbl_barang');

		$data = array(		
			'cart_item' 		=> $cart_item,
			'cart_qty' 			=> $cart_qty,
			'cart_harga' 		=> $cart_harga,
			'cart_jumlah' 		=> $cart_jumlah,
			'cart_kasir' 		=> '2',
			);

			$this->web_app_model->insertData($data,'tbl_cart');
			header('location:'.base_url().'index.php/admin/bg_transaksi?transaksi=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Produk berhasil ditambahkan!
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Produk berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");		
		
	}

	public function bayar()
	{
		date_default_timezone_set('Asia/Jakarta');

		$id_transaksi			= $this->get_id();

		$transaksi_total		= str_replace(".", "", $this->input->post('total_belanja'));
		$transaksi_bayar		= str_replace(".", "", $this->input->post('dibayar'));
		$transaksi_kembalian	= str_replace(".", "", $this->input->post('kembali'));
		$transaksi_stts			= '1';

		$cart_paid 				= '1';
		$cart_date 				= date('Y-m-d H:i:s');

		
		function rupiah($angka){
		    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
		    return $hasil_rupiah;
    	}
    	
		//$cek_harga              = $this->web_app_model->getWhereOneItem($cart_item,'barang_no','tbl_barang');

		$data = array(		
			'transaksi_cart_id' 	=> $id_transaksi,
			'transaksi_total' 		=> $transaksi_total,
			'transaksi_bayar' 		=> $transaksi_bayar,
			'transaksi_kembalian' 	=> $transaksi_kembalian,
			'transaksi_stts' 		=> $transaksi_stts,
			'transaksi_date' 		=> $cart_date,
			);

		if($transaksi_bayar >= $transaksi_total)
		{
			$this->web_app_model->insertData($data,'tbl_transaksi');
			$this->web_app_model->get_id_transaksi($id_transaksi, $cart_paid, $cart_date);
			//$this->web_app_model->pengurangan_stok($cart_qty,$cart_item);

			header('location:'.base_url().'index.php/admin/bg_transaksi?transaksi=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Transaksi CODE-REF".$id_transaksi." dengan total belanja senilai <b>".rupiah($transaksi_total)."</b> telah tercatat dalam database transaksi pembayaran. <b>Selamat atas pendapatan Anda!</b>
															<br><br>
															<a target='_blank' href='".base_url()."index.php/admin/cetak_struk/".$id_transaksi."'><b>Klik disini untuk cetak struk!</b></a>
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
											     swal({
											                title: 'Selamat!!',
											                text:  'Transaksi belanja senilai ".rupiah($transaksi_total)." sudah masuk ke database Anda!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");	


			$pemasukan_hr_ini	= $this->web_app_model->getSUMAllData('tbl_transaksi','transaksi_total','DATE(transaksi_date)',date('Y-m-d'));

			$data_config  		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');

            $tgl1 = new DateTime(date('Y-m-d'));
            $tgl2 = new DateTime($data_config['end_hosting']);
            $jarak = $tgl2->diff($tgl1);

            
         

			// BATAS NOTIF

					$TOKEN  = $this->get_token_bot_telegram();  // ganti token ini dengan token bot mu
					$chatid = $this->get_id_group_telegram(); // ini id saya di telegram @hasanudinhs silakan diganti dan disesuaikan
					$pesan 	= "<b>Selamat atas pencapaian Anda!!</b>\nTerjadi transaksi saat ini senilai ".rupiah($transaksi_total).". Sehingga total pemasukan Anda hari ini menjadi ".rupiah($pemasukan_hr_ini['sum']).". \n\nSisa hosting akan berakhir dalam waktu <b>".$jarak->days." hari</b>\n(Berakhir tanggal ".date('d F Y', strtotime($data_config['end_hosting'])).")";

					// ----------- code -------------

					$method	= "sendMessage";
					$url    = "https://api.telegram.org/bot" . $TOKEN . "/". $method;
					$post = [
					 'chat_id' => $chatid,
					  'parse_mode' => 'HTML', // aktifkan ini jika ingin menggunakan format type HTML, bisa juga diganti menjadi Markdown
					 'text' => $pesan
					];

					$header = [
					 "X-Requested-With: XMLHttpRequest",
					 "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36" 
					];

					// hapus 1 baris ini:
					//die('Hapus baris ini sebelum bisa berjalan, terimakasih.');


					$ch = curl_init();
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_URL, $url);
					//curl_setopt($ch, CURLOPT_REFERER, $refer);
					//curl_setopt($ch, CURLOPT_VERBOSE, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $post );   
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					$datas = curl_exec($ch);
					$error = curl_error($ch);
					$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					curl_close($ch);

					$debug['text'] = $pesan;
					$debug['code'] = $status;
					$debug['status'] = $error;
					$debug['respon'] = json_decode($datas, true);

					/*
					// START NOTIF BOT WA

					$penerima 	= ''.$data_config['hp_owner'].'';
					$hp_server 	= ''.$data_config['hp_server'].'';


					// BOT WA BOTFY TRACER

					$url = "https://botfy.xyz/api/kirim_wa";

					$curl = curl_init($url);
					curl_setopt($curl, CURLOPT_URL, $url);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

					$headers = array(
					   "Accept: application/json",
					   "Authorization: Bearer If1lUK1XRMvpW4tGcGYHv5rwlE0P0bYVDQaZOm0UO1OhIyCel4PGLBWRvSyq",
					   "Content-Type: application/x-www-form-urlencoded",
					);
					curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

					$data_api = "no_wa=".$penerima."&pesan=SISTEM KASIR NOTIFIKASI: Transaksi masuk senilai ".rupiah($transaksi_total).". Total transaksi menjadi ".rupiah($pemasukan_hr_ini['sum'])."&no_device=".$hp_server."";

					curl_setopt($curl, CURLOPT_POSTFIELDS, $data_api);

					//for debug only!
					curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
					curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

					$resp = curl_exec($curl);
					curl_close($curl);
					//var_dump($resp);

					// END BOTFY
					*/
		}
		else
		{
			header('location:'.base_url().'index.php/admin/bg_transaksi?transaksi=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-danger'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-remove'></i>
															Gagal! - 
														</strong>
														Uangnya kurang :(
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
											     swal({
											                title: 'Gagal!!',
											                text:  'Uangnya kurang :(',
											                type: 'warning',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");	
		}

			
	}

	function data_cart(){
        /*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		
		$this->db->select('*');
		$this->db->from('tbl_cart');
		$this->db->join('tbl_barang', 'tbl_barang.barang_no = tbl_cart.cart_item','left');
		$this->db->where('cart_date', '0000-00-00 00:00:00');
		$this->db->order_by('cart_no', 'DESC');

		$total = $this->db->get()->num_rows();
		//$total=$this->db->count_all_results("bookings");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("cart_item",$search);
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/

		//$this->db->order_by('bk_date_of_booking','DESC');
		//$query=$this->db->get('bookings');

		$this->db->select('*');
		$this->db->from('tbl_cart');
		$this->db->join('tbl_barang', 'tbl_barang.barang_no = tbl_cart.cart_item','left');	
		$this->db->where('cart_date', '0000-00-00 00:00:00');
		$this->db->order_by('cart_no', 'DESC');


		$query = $this->db->get();


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("cart_item",$search);
		$jum=$this->db->get('tbl_cart');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		//$kontroller 			= $this->session->userdata('kontroller');

		function rupiah($angka){
		    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
		    return $hasil_rupiah;
    	}

		$nomor_urut=$start+1;
		foreach ($query->result_array() as $cart) {
			$hapus 					= '"Are you sure you want to delete the '.$cart['barang_nama'].'?"';

			if($cart['barang_harga'] == 0)
			{
				$harga = rupiah($cart['cart_harga']);
			}
			else
			{
				$harga = rupiah($cart['barang_harga']);
			}

			if(empty($cart['barang_nama']))
			{
				$barang = $cart['cart_item'];
			}
			else
			{
				$barang = $cart['barang_nama'];
			}

			$output['data'][]=array(
				"#".$cart['cart_no'],
				$barang,
				$harga,
				$cart['cart_qty'],
				rupiah($cart['cart_jumlah']),
				"<a style='background-color: #DC143C;' href='".base_url()."index.php/admin/hapusCart/".$cart['cart_no']."/?transaksi=1' class='btn btn-xs tooltips' data-placement='top' data-original-title='Edit'><font color='white'><i class='fa fa-trash-o'></i></font></a> 
				 
				");
		$nomor_urut++;
		}

		echo json_encode($output);

    }

// BAGIAN TRANSAKSI -- END


// START RIWAYAT TRANSAKSI 

   	public function bg_riwayat_transaksi()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Klinik' || $stts=='Staff Klinik' )
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['total_transaksi']		= $this->web_app_model->getWhereOneItem($this->uri->segment(3),'transaksi_cart_id','tbl_transaksi');

			$bc['total_transaksi_harini']= $this->web_app_model->get_tot_riwatat_transaksi($this->uri->segment(4),$this->uri->segment(5));

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['atas'] 				= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('admin/bio',$bc,true);	
			//$bc['modalTambahPasien'] 	= $this->load->view('admin/modalTambahPasien',$bc,true);	
			$this->load->view('admin/bg_riwayat_transaksi',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function bg_cetak_riwayat_transaksi()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Klinik')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada


			//$bc['data_kategori']		= $this->web_app_model->getAllData('tbl_kategori');
			//$bc['data_barang']		= $this->web_app_model->getAll2Join('barang_kategori','kategori_no','tbl_barang','tbl_kategori');
			//$bc['grafik_kategori']		= $this->web_app_model->grafik_kategori_barang();

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['atas'] 				= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('admin/bio',$bc,true);	
			//$bc['modalTambahPembelianManual'] 	= $this->load->view('admin/modalTambahPembelianManual',$bc,true);
			//$bc['modalTambahBarang'] 	= $this->load->view('admin/modalTambahBarang',$bc,true);
			//$bc['modalEditBarang'] 		= $this->load->view('admin/modalEditBarang',$bc,true);	
			$this->load->view('admin/bg_cetak_riwayat_transaksi',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function filter_transaksi()
	{
		$tgl_awal	= $this->input->post('tgl_awal');
		$tgl_akhir	= $this->input->post('tgl_akhir');

		header('location:'.base_url().'index.php/admin/bg_riwayat_transaksi/0/'.$tgl_awal.'/'.$tgl_akhir.'?transaksi2=1');
	}

    function data_riwayat_transaksi_optimized(){

    	$cart_id    = $this->uri->segment(3); 
    	$tgl_awal   = $this->uri->segment(4); 
    	$tgl_akhir  = $this->uri->segment(5); 

        $draw 		= $this->input->post('draw');
        $search 	= $this->input->post('search')['value'];
        $start 		= $this->input->post('start');
        $length 	= $this->input->post('length');
        $order 		= $this->input->post('order');

		$this->db->from('tbl_transaksi')
					->where('DATE(transaksi_date) >=',''.$tgl_awal.'')
					->where('DATE(transaksi_date) <=',''.$tgl_akhir.'');
			 	// ->join('tbl_kategori', 'tbl_kategori.kategori_no = tbl_barang.barang_kategori');

		if($order){
			foreach ($order as $value) {
				switch ($value['column']) {
					case 0:
						$this->db->order_by('transaksi_date', $value['dir']);
						break;
					case 1:
						$this->db->order_by('transaksi_cart_id', $value['dir']);
						break;
					case 2:
						$this->db->order_by('transaksi_total', $value['dir']);
						break;
				}
			}
		}
			
		if($search!=""){
			$this->db->like("transaksi_date",$search);
			//$this->db->or_like("kategori_nama",$search);
		}

		$tempdb = clone $this->db;
		$output['recordsFiltered'] = $tempdb->count_all_results();

		$this->db->limit($length,$start);

		$data = $this->db->get()->result_array();

		function rupiah($angka){
		    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
		    return $hasil_rupiah;
    	}

    	

		foreach ($data as $d) {
			if($cart_id == $d['transaksi_cart_id'])
	    	{
	    		$color = '#8FBC8F';
	    	}
	    	else
	    	{
	    		$color = 'white';	
	    	}

	    	$status 	= $this->session->userdata('stts');

	    	if($status == 'Admin Klinik')
        	{
        		$transaksi_total = rupiah($d['transaksi_total']);
        	}
        	else
        	{
        		$transaksi_total = '*******';
        	}

			$hapus 					= '"Are you sure you want to delete the REFCODE-'.$d['transaksi_cart_id'].'?"';

			$output['data'][]=array("<span style='background-color:".$color.";'>".date('d-m-Y H:i', strtotime($d['transaksi_date']))." WIB","<span style='background-color:".$color.";'>TRANSCODE-".$d['transaksi_cart_id'],"<span style='background-color:".$color.";'>".$transaksi_total,"
				<a style='background-color: red;' href='".base_url()."index.php/admin/hapus_transaksi/".$d['transaksi_cart_id']."/".$this->uri->segment(4)."/".$this->uri->segment(5)."' class='btn btn-xs space10 tooltips' data-placement='top' onclick='return confirm(".$hapus.")' data-original-title='Hapus'><font color='white'> <i class='fa fa-trash'></i> Hapus</font> </a>
				<a style='background-color: green;' href='".base_url()."index.php/admin/bg_riwayat_transaksi/".$d['transaksi_cart_id']."/".$tgl_awal."/".$tgl_akhir."?transaksi2=1' class='btn btn-xs tooltips' data-placement='top' data-original-title='Edit'><font color='white'>Produk Terjual <i class='clip-arrow-right-3'></i></font> </a>");
		}

		$output['recordsTotal'] = $this->db->from('tbl_transaksi')
											->where('DATE(transaksi_date) >=',''.$tgl_awal.'')
											->where('DATE(transaksi_date) <=',''.$tgl_akhir.'')
											//->join('tbl_kategori', 'tbl_kategori.kategori_no = tbl_barang.barang_kategori')
											->get()->num_rows();

		echo json_encode($output);
    } 

    function data_riwayat_transaksi_all(){

    	//$cart_id    = $this->uri->segment(3); 
    	$tgl_awal   = $this->uri->segment(3); 
    	$tgl_akhir  = $this->uri->segment(4); 

        $draw 		= $this->input->post('draw');
        $search 	= $this->input->post('search')['value'];
        $start 		= $this->input->post('start');
        $length 	= $this->input->post('length');
        $order 		= $this->input->post('order');

		$this->db->from('tbl_cart')
					->where('DATE(cart_date) >=',''.$tgl_awal.'')
					->where('DATE(cart_date) <=',''.$tgl_akhir.'')
					->where('cart_paid','1')
			 		->join('tbl_barang', 'tbl_barang.barang_no = tbl_cart.cart_item', 'left');

		if($order){
			foreach ($order as $value) {
				switch ($value['column']) {
					case 0:
						$this->db->order_by('cart_date', $value['dir']);
						break;
					case 1:
						$this->db->order_by('cart_item', $value['dir']);
						break;
					case 2:
						$this->db->order_by('cart_harga', $value['dir']);
						break;
					case 3:
						$this->db->order_by('cart_qty', $value['dir']);
						break;
					case 4:
						$this->db->order_by('cart_jumlah', $value['dir']);
						break;
				}
			}
		}
			
		if($search!=""){
			$this->db->like("cart_item",$search);
			$this->db->or_like("barang_nama",$search);
		}

		$tempdb = clone $this->db;
		$output['recordsFiltered'] = $tempdb->count_all_results();

		$this->db->limit($length,$start);

		$data = $this->db->get()->result_array();

		function rupiah($angka){
		    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
		    return $hasil_rupiah;
    	}

    	

		foreach ($data as $d) {

			if(!empty($d['barang_nama']))
			{
				$barang  	= $d['barang_nama'];
			}
			else
			{
				$barang 	= $d['cart_item'];
			}

			$hapus 					= '"Are you sure you want to delete transaction at-'.$d['cart_date'].'?"';

			$output['data'][]=array(
				date('d/m/Y H:i', strtotime($d['cart_date']))." WIB",
				$barang,
				rupiah($d['cart_harga']),
				$d['cart_qty'],
				rupiah($d['cart_jumlah']),
			);
		}

		$output['recordsTotal'] = $this->db->from('tbl_cart')
											->where('DATE(cart_date) >=',''.$tgl_awal.'')
											->where('DATE(cart_date) <=',''.$tgl_akhir.'')
											->where('cart_paid','1')
			 								->join('tbl_barang', 'tbl_barang.barang_no = tbl_cart.cart_item', 'left')
											->get()->num_rows();

		echo json_encode($output);
    } 

    public function hapus_transaksi()
	{
		$transaksi_cart_id	= $this->uri->segment(3);
		$tgl_awal			= $this->uri->segment(4);
		$tgl_akhir			= $this->uri->segment(5);
		$hapus_transaksi 	= array('transaksi_cart_id'=>$transaksi_cart_id);
		$hapus_cart			= array('cart_id'=>$transaksi_cart_id);


		$this->web_app_model->deleteData('tbl_transaksi',$hapus_transaksi);
		$this->web_app_model->deleteData('tbl_cart',$hapus_cart);
		header('location:'.base_url().'index.php/admin/bg_riwayat_transaksi/0/'.$tgl_awal.'/'.$tgl_akhir.'?transaksi2=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Riwayat Transaksi has been deleted!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Riwayat Transaksi has been deleted!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

    function data_produk_terjual(){

    	$cart_id      = $this->uri->segment(3);
        /*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		
		$this->db->select('*');
		$this->db->from('tbl_cart');
		$this->db->join('tbl_barang', 'tbl_barang.barang_no = tbl_cart.cart_item', 'left');
		$this->db->where('cart_id',$cart_id);
		$this->db->order_by('cart_no', 'DESC');

		$total = $this->db->get()->num_rows();
		//$total=$this->db->count_all_results("bookings");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("cart_item",$search);
		//$this->db->or_like("transaksi_date",$search);
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/

		//$this->db->order_by('bk_date_of_booking','DESC');
		//$query=$this->db->get('bookings');

		$this->db->select('*');
		$this->db->from('tbl_cart');
		$this->db->join('tbl_barang', 'tbl_barang.barang_no = tbl_cart.cart_item', 'left');
		$this->db->where('cart_id',$cart_id);
		$this->db->order_by('cart_no', 'DESC');


		$query = $this->db->get();


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("cart_item",$search);
		//$this->db->or_like("transaksi_date",$search);
		$jum=$this->db->get('tbl_cart');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

	    function rupiah($angka){
	    
		    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
		    return $hasil_rupiah;
		 
		}
		//$kontroller 			= $this->session->userdata('kontroller');

		$nomor_urut=$start+1;
		foreach ($query->result_array() as $d) {
			$hapus 					= '"Are you sure you want to delete the transaction on '.$d['cart_item'].'?"';

			if(empty($d['barang_nama']))
			{
				$produk = $d['cart_item'];
			}
			else
			{
				$produk = $d['barang_nama'];
			}

			$status 	= $this->session->userdata('stts');
	    	if($status == 'Admin Klinik')
        	{
        		$cart_harga 	= rupiah($d['cart_harga']);
        		$cart_jumlah 	= rupiah($d['cart_jumlah']);
        	}
        	else
        	{
        		$cart_harga 	= '*****';
        		$cart_jumlah 	= '*****';
        	}

			$output['data'][]=array("<p align='center'>".$nomor_urut.".</p>",$produk,$cart_harga,"<p align='center'>".$d['cart_qty']."</p>",$cart_jumlah);
		$nomor_urut++;
		}

		echo json_encode($output);

    }




// END RIWAYAT TRANSAKSI

// BAGIAN BARANG -- START

	public function bg_cetak_riwayat_pembelian()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Klinik')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada


			$bc['data_kategori']		= $this->web_app_model->getAllData('tbl_kategori');
			$bc['data_barang']		= $this->web_app_model->getAll2Join('barang_kategori','kategori_no','tbl_barang','tbl_kategori');
			//$bc['grafik_kategori']		= $this->web_app_model->grafik_kategori_barang();

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['atas'] 				= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('admin/bio',$bc,true);	
			//$bc['modalTambahPembelianManual'] 	= $this->load->view('admin/modalTambahPembelianManual',$bc,true);
			//$bc['modalTambahBarang'] 	= $this->load->view('admin/modalTambahBarang',$bc,true);
			//$bc['modalEditBarang'] 		= $this->load->view('admin/modalEditBarang',$bc,true);	
			$this->load->view('admin/bg_cetak_riwayat_pembelian',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}


	public function bg_barang()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Klinik' || $stts=='Staff Klinik')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada


			$bc['data_kategori']		= $this->web_app_model->getAllData('tbl_kategori');
			$bc['data_barang']		= $this->web_app_model->getAll2Join('barang_kategori','kategori_no','tbl_barang','tbl_kategori');
			$bc['grafik_kategori']		= $this->web_app_model->grafik_kategori_barang();

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['atas'] 				= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('admin/bio',$bc,true);	
			$bc['modalTambahPembelianManual'] 	= $this->load->view('admin/modalTambahPembelianManual',$bc,true);
			$bc['modalTambahBarang'] 	= $this->load->view('admin/modalTambahBarang',$bc,true);
			$bc['modalEditBarang'] 		= $this->load->view('admin/modalEditBarang',$bc,true);	
			$this->load->view('admin/bg_barang',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function tambahBarang()
	{

		$barang_nama		= $this->input->post('barang_nama');
		$barang_stok		= $this->input->post('barang_stok');
		$barang_harga		= str_replace(".", "", $this->input->post('barang_harga'));
		$barang_satuan		= $this->input->post('barang_satuan');
		$barang_kategori	= $this->input->post('barang_kategori');

			$data = array(		
				'barang_nama' 		=> $barang_nama,
				'barang_stok'		=> $barang_stok,
				'barang_satuan'		=> $barang_satuan,
				'barang_kategori'	=> $barang_kategori,
				'barang_harga'		=> $barang_harga,
				);
			
			$this->web_app_model->insertData($data,'tbl_barang');
			header('location:'.base_url().'index.php/admin/bg_barang/?barang=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p><font color='black'>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Barang berhasil ditambahkan!</font>
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Barang berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function tambahPembelianManual()
	{

		$keluar_produk		= $this->input->post('keluar_produk');
		$keluar_date		= $this->input->post('keluar_date');
		$keluar_hg_beli		= str_replace(".", "", $this->input->post('keluar_hg_beli'));
		$keluar_qty			= $this->input->post('keluar_qty');

			$data = array(		
				'keluar_produk' 	=> $keluar_produk,
				'keluar_date'		=> $keluar_date,
				'keluar_hg_beli'	=> $keluar_hg_beli,
				'keluar_qty'		=> $keluar_qty,
				);
			
			$this->web_app_model->insertData($data,'tbl_pengeluaran');
			header('location:'.base_url().'index.php/admin/bg_barang/?barang=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p><font color='black'>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Catatan Pembelian Manual berhasil ditambahkan!</font>
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Catatan Pembelian Manual berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}


	public function pembelianBarang()
	{

		$keluar_produk		= $this->input->post('keluar_produk');
		$keluar_date		= $this->input->post('keluar_date');
		$keluar_hg_beli		= str_replace(".", "", $this->input->post('keluar_hg_beli'));
		$keluar_qty			= $this->input->post('keluar_qty');

			$data = array(		
				'keluar_produk' 	=> $keluar_produk,
				'keluar_date'		=> $keluar_date,
				'keluar_hg_beli'	=> $keluar_hg_beli,
				'keluar_qty'		=> $keluar_qty,
				);
			

			$cek_barang				= $this->web_app_model->getWhereOneItem($keluar_produk,'barang_no','tbl_barang');

			if(!empty($cek_barang))
			{
				$this->web_app_model->penambahan_stok($keluar_qty,$keluar_produk);
			}

			$this->web_app_model->insertData($data,'tbl_pengeluaran');
			header('location:'.base_url().'index.php/admin/bg_barang/?barang=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p><font color='black'>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Pembelian Stok Barang berhasil ditambahkan!</font>
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Pembelian Barang berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	function data_pembelian_optimized(){

        $draw 		= $this->input->post('draw');
        $search 	= $this->input->post('search')['value'];
        $start 		= $this->input->post('start');
        $length 	= $this->input->post('length');
        $order 		= $this->input->post('order');

		$this->db->from('tbl_pengeluaran')
			 	 ->join('tbl_barang', 'tbl_barang.barang_no = tbl_pengeluaran.keluar_produk', 'left');

		if($order){
			foreach ($order as $value) {
				switch ($value['column']) {
					case 0:
						$this->db->order_by('keluar_no', $value['dir']);
						break;
					case 1:
						$this->db->order_by('keluar_produk', $value['dir']);
						break;
					case 2:
						$this->db->order_by('keluar_qty', $value['dir']);
						break;
					case 3:
						$this->db->order_by('keluar_hg_beli', $value['dir']);
						break;
				}
			}
		}
			
		if($search!=""){
			$this->db->like("keluar_produk",$search);
			//$this->db->or_like("kategori_nama",$search);
		}

		$tempdb = clone $this->db;
		$output['recordsFiltered'] = $tempdb->count_all_results();

		$this->db->limit($length,$start);

		$data = $this->db->get()->result_array();

		function rupiah($angka){
		    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
		    return $hasil_rupiah;
    	}

		foreach ($data as $d) {
			

			if(empty($d['barang_nama']))
	    	{
	    		$produk = $d['keluar_produk'];
	    	}
	    	else
	    	{
	    		$produk = $d['barang_nama'];
	    	}

			$hapus 					= '"Are you sure you want to delete the '.$produk.'?"';

			$output['data'][]=array(date('d-m-Y', strtotime($d['keluar_date'])),$produk,$d['keluar_qty'],rupiah($d['keluar_hg_beli']),"
				<a style='background-color: #DC143C;' href='".base_url()."index.php/admin/hapusPembelian/".$d['keluar_no']."/".$d['keluar_produk']."/".$d['keluar_qty']."' class='btn btn-xs tooltips' data-placement='top' onclick='return confirm(".$hapus.")'><font color='white'><i class='fa fa-trash'></i></font></a>");
		}

		$output['recordsTotal'] = $this->db->from('tbl_pengeluaran')
			 					 			->join('tbl_barang', 'tbl_barang.barang_no = tbl_pengeluaran.keluar_produk', 'left')
											->get()->num_rows();

		echo json_encode($output);
    } 

    public function hapusPembelian()
	{
		$keluar_no		= $this->uri->segment(3);
		$keluar_produk	= $this->uri->segment(4);
		$keluar_qty		= $this->uri->segment(5);
		$hapus 			= array('keluar_no'=>$keluar_no);

		$cek_barang				= $this->web_app_model->getWhereOneItem($keluar_produk,'barang_no','tbl_barang');

		if(!empty($cek_barang))
		{
			$this->web_app_model->pengurangan_stok($keluar_qty,$keluar_produk);
		}

		$this->web_app_model->deleteData('tbl_pengeluaran',$hapus);
		header('location:'.base_url().'index.php/admin/bg_barang?barang=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Pembelian has been deleted!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Pembelian has been deleted!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

    function data_barang_optimized(){
        $draw 		= $this->input->post('draw');
        $search 	= $this->input->post('search')['value'];
        $start 		= $this->input->post('start');
        $length 	= $this->input->post('length');
        $order 		= $this->input->post('order');

		$this->db->from('tbl_barang')
			 	 ->join('tbl_kategori', 'tbl_kategori.kategori_no = tbl_barang.barang_kategori');

		if($order){
			foreach ($order as $value) {
				switch ($value['column']) {
					case 0:
						$this->db->order_by('barang_no', $value['dir']);
						break;
					case 1:
						$this->db->order_by('barang_nama', $value['dir']);
						break;
					case 2:
						$this->db->order_by('barang_stok', $value['dir']);
						break;
					case 3:
						$this->db->order_by('barang_harga', $value['dir']);
						break;
					case 4:
						$this->db->order_by('barang_satuan', $value['dir']);
						break;
					case 5:
						$this->db->order_by('kategori_nama', $value['dir']);
						break;
					default:
						break;
				}
			}
		}
			
		if($search!=""){
			$this->db->like("barang_nama",$search);
			//$this->db->or_like("kategori_nama",$search);
		}

		$tempdb = clone $this->db;
		$output['recordsFiltered'] = $tempdb->count_all_results();

		$this->db->limit($length,$start);

		$data = $this->db->get()->result_array();

		function rupiah($angka){
		    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
		    return $hasil_rupiah;
    	}

		foreach ($data as $barang) {
			$hapus 					= '"Are you sure you want to delete the '.$barang['barang_nama'].'?"';

			$output['data'][]=array("#".$barang['barang_no'],$barang['barang_nama'],$barang['barang_stok'],rupiah($barang['barang_harga']),$barang['barang_satuan'],$barang['kategori_nama'],"
				
				<a style='background-color: green;' data-toggle='modal' data-target='#modalEditBarang".$barang['barang_no']."' class='btn btn-xs tooltips' data-placement='top' data-original-title='Edit'><font color='white'><i class='fa fa-pencil-square-o'></i></font></a> 
				<a style='background-color: #DC143C;' href='".base_url()."index.php/admin/hapusBarang/".$barang['barang_no']."' class='btn btn-xs tooltips' data-placement='top' onclick='return confirm(".$hapus.")'><font color='white'><i class='fa fa-trash'></i></font></a>");
		}

		$output['recordsTotal'] = $this->db->from('tbl_barang')
											->join('tbl_kategori', 'tbl_kategori.kategori_no = tbl_barang.barang_kategori')
											->get()->num_rows();

		echo json_encode($output);
    } 

    public function editBarang()
	{		
		$barang_no			= $this->input->post('barang_no');
		$barang_nama		= $this->input->post('barang_nama');
		$barang_stok		= $this->input->post('barang_stok');
		$barang_harga		= str_replace(".", "", $this->input->post('barang_harga'));
		$barang_satuan		= $this->input->post('barang_satuan');
		$barang_kategori	= $this->input->post('barang_kategori');

		$data = array(		
			'barang_nama' 		=> $barang_nama,
			'barang_stok' 		=> $barang_stok,
			'barang_harga' 		=> $barang_harga,
			'barang_satuan' 	=> $barang_satuan,
			'barang_kategori' 	=> $barang_kategori,
			);

		$where = array(		
			'barang_no' 	=> $barang_no,
			);
		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_barang');
		header('location:'.base_url().'index.php/admin/bg_barang?barang=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data barang berhasil diupdate !
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data barang berhasil diupdate !',
											                type: 'success',
											                timer: 50000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapusBarang()
	{
		$barang_no		= $this->uri->segment(3);
		$hapus 			= array('barang_no'=>$barang_no);


		$this->web_app_model->deleteData('tbl_barang',$hapus);
		header('location:'.base_url().'index.php/admin/bg_barang?barang=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Barang has been deleted!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Barang has been deleted!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

// BAGIAN BARANG -- END


// BAGIAN PASIEN --- START 

	public function bg_pasien()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Klinik' || $stts=='Staff Klinik')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['data_pasien']		= $this->web_app_model->getWhereOneItem($this->uri->segment(3),'pasien_nik','tbl_pasien');

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['atas'] 				= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('admin/bio',$bc,true);	
			$bc['modalTambahPasien'] 	= $this->load->view('admin/modalTambahPasien',$bc,true);	
			$this->load->view('admin/bg_pasien',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function bg_edit_pasien()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Klinik')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['data_pasien']			= $this->web_app_model->getWhereOneItem($this->uri->segment(3),'pasien_nik','tbl_pasien');

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['atas'] 				= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('admin/bio',$bc,true);		
			$this->load->view('admin/bg_edit_pasien',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function bg_rekam_medis()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Klinik')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			// KHUSUS HALAMAN INI U TAB ======================
			if (isset($_GET['tab1']) && $_GET['tab1']==1) 
			{
				$this->session->set_flashdata("tab1","in active");
				$this->session->set_flashdata("subtab1","active");
			}

			if (isset($_GET['tab2']) && $_GET['tab2']==1) 
			{
				$this->session->set_flashdata("tab2","in active");
				$this->session->set_flashdata("subtab2","active");
			}

			if (isset($_GET['tab3']) && $_GET['tab3']==1) 
			{
				$this->session->set_flashdata("tab3","in active");
				$this->session->set_flashdata("subtab3","active");
			}
			// END=============================================


			$bc['data_pasien']			= $this->web_app_model->getWhereOneItem($this->uri->segment(3),'pasien_nik','tbl_pasien');

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['atas'] 				= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('admin/bio',$bc,true);		

			$bc['riwayat_hidup'] 		= $this->load->view('admin/riwayat_hidup',$bc,true);	

			$this->load->view('admin/bg_rekam_medis',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

    function data_pasien_optimized(){
    	$pasien_nik	= $this->uri->segment(3);

        $draw 		= $this->input->post('draw');
        $search 	= $this->input->post('search')['value'];
        $start 		= $this->input->post('start');
        $length 	= $this->input->post('length');
        $order 		= $this->input->post('order');

		$this->db->from('tbl_pasien');
			 
		if($order){
			foreach ($order as $value) {
				switch ($value['column']) {
					case 0:
						$this->db->order_by('pasien_nama', $value['dir']);
						break;
					case 1:
						$this->db->order_by('pasien_kelamin', $value['dir']);
						break;
					case 2:
						$this->db->order_by('pasien_tgl_lhr', $value['dir']);
						break;
				}
			}
		}
			
		if($search!=""){
			$this->db->like("pasien_nama",$search);
			$this->db->or_like("pasien_nik",$search);
		}

		$tempdb = clone $this->db;
		$output['recordsFiltered'] = $tempdb->count_all_results();

		$this->db->limit($length,$start);

		$data = $this->db->get()->result_array();
		function hitung_umur($tanggal_lahir)
			{
				$birthDate = new DateTime($tanggal_lahir);
				$today = new DateTime("today");
				if ($birthDate > $today) 
				{ 
				    exit("0 tahun 0 bulan 0 hari");
				}
				$y = $today->diff($birthDate)->y;
				$m = $today->diff($birthDate)->m;
				$d = $today->diff($birthDate)->d;
				return $y." tahun ".$m." bulan";
			}

		foreach ($data as $d) {
			if($pasien_nik == $d['pasien_nik'])
	    	{
	    		$color = '#8FBC8F';
	    	}
	    	else
	    	{
	    		$color = 'white';	
	    	}

			$hapus 					= '"Are you sure you want to delete '.$d['pasien_nama'].'?"';

			$output['data'][]=array("<span style='background-color:".$color.";'>Since <br>".date('d-m-Y', strtotime($d['pasien_created']))."</span>","<span style='background-color:".$color.";'>ID_".$d['pasien_nik']."<br><b style='text-transform: uppercase;'>".$d['pasien_nama']."</b></span>","<span style='background-color:".$color.";'>".$d['pasien_kelamin']."</span>","<span style='background-color:".$color.";'>".$d['pasien_tgl_lhr']."<br>(".hitung_umur($d['pasien_tgl_lhr']).")</span>","
				
				<a style='background-color: green;' href='".base_url()."index.php/admin/bg_edit_pasien/".$d['pasien_nik']."?pasien=1' class='btn btn-xs space10 tooltips' data-placement='top' data-original-title='Edit'><font color='white'><i class='fa fa-pencil-square-o'></i></font></a> 
				<a style='background-color: #DC143C;' href='".base_url()."index.php/admin/hapus_pasien/".$d['pasien_nik']."' class='btn btn-xs space10 tooltips' data-placement='top' onclick='return confirm(".$hapus.")'><font color='white'><i class='fa fa-trash'></i> </font></a><br>
				<a style='background-color: #44a2d2;' href='".base_url()."index.php/admin/bg_pasien/".$d['pasien_nik']."?pasien=1' class='btn btn-xs tooltips' data-placement='top' data-original-title='Edit'><font color='white'><i class='clip-file-check'></i> Note</font></a> ");
		}

		$output['recordsTotal'] = $this->db->from('tbl_pasien')
											->get()->num_rows();

		echo json_encode($output);
    } 

    function data_medis_optimized(){
    	
    	$pasien_nik = $this->uri->segment(3);

        $draw 		= $this->input->post('draw');
        $search 	= $this->input->post('search')['value'];
        $start 		= $this->input->post('start');
        $length 	= $this->input->post('length');
        $order 		= $this->input->post('order');

		$this->db->from('tbl_rekam_medis')
				->join('tbl_pasien', 'tbl_pasien.pasien_nik = tbl_rekam_medis.medis_nik')
				->where('medis_nik',$pasien_nik);
			 
		if($order){
			foreach ($order as $value) {
				switch ($value['column']) {
					case 0:
						$this->db->order_by('medis_date', $value['dir']);
						break;
				}
			}
		}
			
		if($search!=""){
			$this->db->like("pasien_nama",$search);
			$this->db->or_like("medis_date",$search);
		}

		$tempdb = clone $this->db;
		$output['recordsFiltered'] = $tempdb->count_all_results();

		$this->db->limit($length,$start);

		$data = $this->db->get()->result_array();

		$no = 1;
		foreach ($data as $d) {
			$hapus 					= '"Are you sure you want to delete '.$d['pasien_nama'].'?"';

			$output['data'][]=array("<p align='center'>".$no.".</p>","<p align='center'>".date('d-m-Y', strtotime($d['medis_date']))."</p>","<b>RIWAYAT BEROBAT: </b><br>".$d['medis_note'],"<p align='center'><a style='background-color: #DC143C;' href='".base_url()."index.php/admin/hapus_data_medis/".$d['medis_no']."/".$d['medis_nik']."' class='btn btn-xs space10 tooltips' data-placement='top' onclick='return confirm(".$hapus.")'><font color='white'><i class='fa fa-trash'></i> </font></a></p>");
			$no++;
		}

		$output['recordsTotal'] = $this->db->from('tbl_rekam_medis')
											->join('tbl_pasien', 'tbl_pasien.pasien_nik = tbl_rekam_medis.medis_nik')
											->where('medis_nik',$pasien_nik)
											->get()->num_rows();

		echo json_encode($output);
    } 

    public function tambah_catatan_medis()
	{
		date_default_timezone_set('Asia/Jakarta');

		$medis_nik			= $this->input->post('medis_nik');
		$medis_date			= $this->input->post('medis_date');
		$medis_note			= $this->input->post('medis_note');


			$data = array(		
				'medis_nik' 		=> $medis_nik,
				'medis_date'		=> $medis_date,
				'medis_note'		=> $medis_note,
				);
		
		if(!empty($medis_nik))
		{
			$this->web_app_model->insertData($data,'tbl_rekam_medis');
			header('location:'.base_url().'index.php/admin/bg_pasien/'.$medis_nik.'?pasien=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p><font color='black'>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Catatan medis has been added!</font>
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Catatan Medis has been added!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
		}
		else
		{
			header('location:'.base_url().'index.php/admin/bg_pasien/'.$medis_nik.'?pasien=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-danger'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p><font color='black'>
														<strong>
															<i class='icon-ok'></i>
															Failed! - 
														</strong>
														Pastikan memilih pasien terlebih dahulu!</font>
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Failed!!',
											                text:  'Pastikan memilih pasien terlebih dahulu!',
											                type: 'warning',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
		}
			
	}

	public function hapus_data_medis()
	{
		$medis_no		= $this->uri->segment(3);
		$pasien_nik		= $this->uri->segment(4);
		$hapus 			= array('medis_no'=>$medis_no);


		$this->web_app_model->deleteData('tbl_rekam_medis',$hapus);
		header('location:'.base_url().'index.php/admin/bg_pasien/'.$pasien_nik.'?pasien=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Catatan Medis has been deleted!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Catatan Medis has been deleted!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}



    public function tambahPasien()
	{
		date_default_timezone_set('Asia/Jakarta');

		$pasien_nik			= $this->input->post('pasien_nik');
		$pasien_nama		= $this->input->post('pasien_nama');
		$pasien_tgl_lhr		= $this->input->post('pasien_tgl_lhr');
		$pasien_kelamin		= $this->input->post('pasien_kelamin');

			$data = array(		
				'pasien_nik' 		=> $pasien_nik,
				'pasien_nama'		=> $pasien_nama,
				'pasien_tgl_lhr'	=> $pasien_tgl_lhr,
				'pasien_kelamin'	=> $pasien_kelamin,
				'pasien_created'	=> date('Y-m-d'),
				);
			
			$this->web_app_model->insertData($data,'tbl_pasien');
			header('location:'.base_url().'index.php/admin/bg_pasien/?pasien=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p><font color='black'>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Pasien has been added!</font>
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Pasien has been added!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_pasien()
	{
		$pasien_nik		= $this->uri->segment(3);
		$hapus 			= array('pasien_nik'=>$pasien_nik);


		$this->web_app_model->deleteData('tbl_pasien',$hapus);
		header('location:'.base_url().'index.php/admin/bg_pasien?pasien=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Pasien has been deleted!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Pasien has been deleted!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function updatePasien()
	{		
		$pasien_nik			= $this->input->post('pasien_nik');
		$pasien_nama		= $this->input->post('pasien_nama');
		$pasien_tgl_lhr		= $this->input->post('pasien_tgl_lhr');
		$pasien_kelamin		= $this->input->post('pasien_kelamin');

		$data = array(		
			'pasien_nama' 		=> $pasien_nama,
			'pasien_tgl_lhr' 	=> $pasien_tgl_lhr,
			'pasien_kelamin' 	=> $pasien_kelamin,
			);

		$where = array(		
			'pasien_nik' 		=> $pasien_nik,
			);
		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_pasien');
		header('location:'.base_url().'index.php/admin/bg_pasien?pasien=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data pasien berhasil diupdate !
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data pasien berhasil diupdate !',
											                type: 'success',
											                timer: 50000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

// BAGIAN PASIEN ---- END




}
