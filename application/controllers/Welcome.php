<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	/**
	 * Created by Ilham Ramadhan, S.Tr.Kom
	 * IT PDAM Kota langsa
	 * HP : 0853 6188 5100
	 * Email: ilhamr6000@gmail.com
	 */
	public function _construct()
	{
		session_start();
	}

	public function index()
	{

		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(empty($cek))
		{
			// CONFIG ============
			$bc['data_config']	= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ========
			
			$this->load->view('tampilan_login', $bc);
		}
		else
		{
			if($stts == 'Admin Klinik' || $stts == 'Staff Klinik' )
			{
				header('location:'.base_url().'index.php/admin');
			}
		}
	}

	public function login()
	{
		$u = $this->input->post('username');
		$p = $this->input->post('password');
		$this->web_app_model->getLoginData($u, $p);
	}
	
	public function logout()
	{
		$cek  = $this->session->userdata('logged_in');
		if(empty($cek))
		{
			header('location:'.base_url().'index.php/welcome');
		}
		else
		{
			$this->session->sess_destroy();
			header('location:'.base_url().'index.php/welcome');
		}
	}
}
