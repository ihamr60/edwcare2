<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web_App_Model extends CI_Model 
{	
	// db2 digunakan untuk mengakses database ke-2
	// private $db2;

	/* public function __construct()
	 {
	  parent::__construct();
	         $this->db2 = $this->load->database('db2', TRUE);
	 }
	 */

	 private $db2;

	 public function __construct()
	 {
	  parent::__construct();
	         $this->db2 = $this->load->database('db2', TRUE);
	 }

	public function getLoginData($usr, $psw) 
	{
		$u = $usr; 
		$p = md5($psw);
		
		$q_cek_login = $this->db->get_where('tbl_user', array('user_username' => $u, 'user_pwd' => $p)); 
		if(count($q_cek_login->result())>0)
		{
			foreach ($q_cek_login->result() as $qck)
			{			
				if($qck->user_role=='2')
				{
					$q_ambil_data = $this->db->get_where('tbl_user', array('user_username' => $u));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 = 'yes';
						$sess_data['username'] 		 = $qad->user_username;
						$sess_data['nama'] 		 	 = $qad->user_nama;
						$sess_data['stts'] 			 = 'Admin Klinik';
					    $this->session->set_userdata($sess_data);  
					}	
					header('location:'.base_url().'index.php/admin');
				}
				else if($qck->user_role=='3')
				{
					$q_ambil_data = $this->db->get_where('tbl_user', array('user_username' => $u));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 = 'yes';
						$sess_data['username'] 		 = $qad->user_username;
						$sess_data['nama'] 		 	 = $qad->user_nama;
						$sess_data['stts'] 			 = 'Staff Klinik';
					    $this->session->set_userdata($sess_data);  
					}	
					header('location:'.base_url().'index.php/admin/bg_transaksi');
				}
			}
		}
		else
			{
				header('location:'.base_url().'index.php/welcome');
				$this->session->set_flashdata('info','<div class="alert alert-danger">
					                                    <button data-dismiss="alert" class="close">
					                                        &times;
					                                    </button>
					                                    <i class="fa fa-times-circle"></i>
					                                    <strong>Maaf!</strong> Username atau password anda salah.
					                                </div>');

				$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
												     swal({
												                title: 'Gagal Login!!',
												                text:  'Password / Username salah!',
												                type: 'warning',
												                timer: 3000,
												                showConfirmButton: true
												            });  
												     },10);  
												    </script>
												    ");
			}

		//$this->db->close();
	}

	public function updateDataWhere($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
		return;
		$this->db->close();
	}

	public function insertData($data,$table)
	{
		$this->db->insert($table,$data);
		return;
		$this->db->close();
	}

	public function getWhereOneItem($where,$field,$table)
	{
		$data = array();
  		$options = array($field => $where);
  		$Q = $this->db->get_where($table,$options);
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;

  		$this->db->close();
	}

	public function get2WhereOneItem($where,$field,$where2,$field2,$table)
	{
		$data = array();
  		$options = array($field => $where, $field2 => $where2 );
  		$Q = $this->db->get_where($table,$options);
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;

  		$this->db->close();
	}

	public function get2WhereCOUNTOneItem($where,$field,$table, $count, $field2, $where2, $field3, $where3)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(".$count.") AS TOTAL FROM ".$table." WHERE ".$field."='".$where."' AND ".$field2." = '".$where2."' AND ".$field3." = '".$where3."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;

  		$this->db->close();
	}

	public function get1WhereCOUNTOneItem($where,$field,$table, $count, $field2, $where2)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(".$count.") AS TOTAL FROM ".$table." WHERE ".$field."='".$where."' AND ".$field2." = '".$where2."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;

  		$this->db->close();
	}

	public function get2WhereOneItemOrder($where,$field,$where2,$field2,$table,$fieldOrder,$order)
	{
		$data = array();
		$this->db->where($field,$where);
		$this->db->where($field2,$where2);
		$this->db->order_by($fieldOrder, $order);
		$Q = $this->db->get($table);
			if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
  		$this->db->close();
	}

	public function getAllData($table)
	{
		return $this->db->get($table);
		//return $this->db->get($table)->result();

		$this->db->close();
	}

	public function getSUMAllData_nowhere($table, $field)
	{
		$data = array();
		$Q = $this->db->query("SELECT SUM(".$field.") as sum FROM ".$table."");
			if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
  		$this->db->close();
	}

	public function getSUMAllData($table, $field, $field2, $where2)
	{
		$data = array();
		$Q = $this->db->query("SELECT SUM(".$field.") as sum FROM ".$table." WHERE ".$field2." = '".$where2."'");
			if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
  		$this->db->close();
	}

	public function getSUMAllData2($table, $field, $field2, $where2, $field3, $where3)
	{
		$data = array();
		$Q = $this->db->query("SELECT SUM($field) as sum FROM $table WHERE $field2 = $where2 AND $field3 = $where3");
			if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
  		$this->db->close();
	}

	public function get_tot_riwatat_transaksi($tgl_awal, $tgl_akhir)
	{
		$data = array();
		$Q = $this->db->query("SELECT SUM(transaksi_total) as sum FROM tbl_transaksi WHERE DATE(transaksi_date) >= '".$tgl_awal."' AND DATE(transaksi_date) <= '".$tgl_akhir."'");
			if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
  		$this->db->close();
	}

	public function getSUMAllData3($table, $field, $field2, $where2, $field3, $where3, $field4, $where4)
	{
		$data = array();
		$Q = $this->db->query("SELECT SUM($field) as sum FROM $table WHERE $field2 = $where2 AND $field3 = $where3 AND $field4 = $where4");
			if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
  		$this->db->close();
	}

	public function getCOUNTAllData3($table, $field, $field2, $where2, $field3, $where3, $field4, $where4)
	{
		$data = array();
		$Q = $this->db->query("SELECT COUNT($field) as count FROM $table WHERE $field2 = $where2 AND $field3 = $where3 AND $field4 = $where4");
			if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
  		$this->db->close();
	}

	public function getCOUNTAllData($table, $field, $field2, $where2)
	{
		$data = array();
		$Q = $this->db->query("SELECT COUNT($field) as count FROM $table WHERE $field2 = $where2");
			if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
  		$this->db->close();
	}

	public function transaksi_group_date()
	{
		return $this->db->query("SELECT transaksi_date, SUM(transaksi_total) as sum FROM tbl_transaksi WHERE YEAR(transaksi_date)= '".date('Y')."' AND MONTH(transaksi_date)='".date('m')."' GROUP BY DATE(transaksi_date)");
  		$this->db->close();
	}

	public function pengeluaran_group_date()
	{
		return $this->db->query("SELECT keluar_date, SUM(keluar_hg_beli) as sum FROM tbl_pengeluaran WHERE YEAR(keluar_date)= '".date('Y')."' AND MONTH(keluar_date)='".date('m')."' GROUP BY DATE(keluar_date)");
  		$this->db->close();
	}

	public function transaksi_per_transaksi()
	{
		return $this->db->query("SELECT * FROM tbl_transaksi WHERE DATE(transaksi_date)= '".date('Y-m-d')."'");
  		$this->db->close();
	}

	public function deleteData($table,$data)
	{
		$this->db->delete($table, $data);
		return;
		$this->db->close();
	}

	public function getWhereAllItem($where,$field,$table)
	{
		$this->db->where($field,$where);
		return $this->db->get($table);
		$this->db->close();
	}

	public function getWhereLimit($where,$field,$table,$limit)
	{
		$this->db->where($field,$where);
		$this->db->limit($limit);
		return $this->db->get($table);
		$this->db->close();
	}

	public function cek_atasan_terakhir($nik_pegawai)
	{
		$data = array();
		$Q = $this->db->query("SELECT * FROM ekin_kinerja WHERE nik_pegawai ='".$nik_pegawai."' ORDER BY no DESC");
			if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
  		$this->db->close();
	}

	public function jumlah_ekin($tgl_awal,$tgl_akhir,$nik_pegawai)
	{
		return $this->db->query("SELECT COUNT(nik_pegawai) AS total_ekin FROM ekin_kinerja WHERE DATE(updated_at) BETWEEN '".$tgl_awal."' AND '".$tgl_akhir."' AND nik_pegawai='".$nik_pegawai."' GROUP BY DATE(updated_at)");
  		$this->db->close();
	}

	public function pengurangan($tgl_awal,$tgl_akhir,$nik_pegawai)
	{
		return $this->db->query("SELECT COUNT(nik_pegawai) AS total_ekin FROM ekin_kinerja WHERE DATE(updated_at) BETWEEN '".$tgl_awal."' AND '".$tgl_akhir."' AND nik_pegawai='".$nik_pegawai."' GROUP BY DATE(updated_at)");
  		$this->db->close();
	}

	public function grafik_kategori_barang()
	{
		return $this->db->query("SELECT kategori_nama, kategori_no, COUNT(barang_no) as total FROM `tbl_barang` INNER JOIN tbl_kategori ON tbl_barang.barang_kategori = tbl_kategori.kategori_no GROUP BY barang_kategori");
  		$this->db->close();
	}

	public function pengurangan_stok($n, $barang_no)
	{
		return $this->db->query("UPDATE tbl_barang SET barang_stok = barang_stok-$n WHERE barang_no=$barang_no");
  		$this->db->close();
	}

	public function penambahan_stok($n, $barang_no)
	{
		return $this->db->query("UPDATE tbl_barang SET barang_stok = barang_stok+$n WHERE barang_no=$barang_no");
  		$this->db->close();
	}

	public function get_id_transaksi($id, $paid, $date)
	{
		return $this->db->query("UPDATE tbl_cart SET cart_id = '$id', cart_paid = '$paid', cart_date = '$date' WHERE cart_paid = '0'");
  		$this->db->close();
	}

	public function getWhereAllItemGroup($where,$field,$table,$group)
	{
		$this->db->where($field,$where);
		$this->db->group_by($group);
		return $this->db->get($table);
		$this->db->close();
	}

	public function getWhereAllItemOrder($where,$field,$table,$field2,$order)
	{
		$this->db->where($field,$where);
		$this->db->order_by($field2, $order);
		return $this->db->get($table);
		$this->db->close();
	}

	public function get2WhereAllItemOrder($where,$field,$where2,$field2,$table,$fieldOrder,$order)
	{
		$this->db->where($field,$where);
		$this->db->where($field2,$where2);
		$this->db->order_by($fieldOrder, $order);
		return $this->db->get($table);
		$this->db->close();
	}

	public function getAll2Join($idTabel1,$idTabel2,$table1,$table2)
	{
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
		$this->db->close();
	}

	public function getWhereAll2Join($idTabel1,$idTabel2,$table1,$table2,$field1,$where1)
	{
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$this->db->where($field1,$where1);
		$query = $this->db->get();
		return $query;
		$this->db->close();
	}

	public function getWhereAll2Join_left($idTabel1,$idTabel2,$table1,$table2,$field1,$where1)
	{
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'', 'left');
		$this->db->from($table1);
		$this->db->where($field1,$where1);
		$query = $this->db->get();
		return $query;
		$this->db->close();
	}

	public function getWhereAll2JoinGroup($idTabel1,$idTabel2,$table1,$table2,$field1,$where1,$group)
	{
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$this->db->where($field1,$where1);
		$this->db->group_by($group);
		$query = $this->db->get();
		return $query;
		$this->db->close();
	}


	public function getDataPegawai($kateg_peg)
	{
		 $this->db->select('*');
		 $this->db->from('tbl_pegawai');
		 $this->db->join('tbl_jabatan','tbl_jabatan.id_jabatan = tbl_pegawai.jabatan');
		 $this->db->join('tbl_golongan','tbl_golongan.id_pangkat = tbl_pegawai.golongan');
		 $this->db->join('tbl_bagian','tbl_bagian.id_bagian = tbl_pegawai.bagian');
		 $this->db->join('tbl_kateg_peg','tbl_kateg_peg.uuid_kateg_peg = tbl_pegawai.kateg_pegawai');
		 $this->db->where('kateg_pegawai',$kateg_peg);
		 $query = $this->db->get();
		 return $query;
		 $this->db->close();
	}

	public function getDataAllPegawai()
	{
		 $this->db->select('*');
		 $this->db->from('tbl_pegawai');
		 $this->db->join('tbl_jabatan','tbl_jabatan.id_jabatan = tbl_pegawai.jabatan');
		 $this->db->join('tbl_golongan','tbl_golongan.id_pangkat = tbl_pegawai.golongan');
		 $this->db->join('tbl_bagian','tbl_bagian.id_bagian = tbl_pegawai.bagian');
		 $this->db->join('tbl_kateg_peg','tbl_kateg_peg.uuid_kateg_peg = tbl_pegawai.kateg_pegawai');
		// $this->db->where('kateg_pegawai',$kateg_peg);
		 $query = $this->db->get();
		 return $query;
		 $this->db->close();
	}

	public function getDataPegawai_oneItem($nik_ktp)
	{
		$data 		= array();
  					  $this->db->select('*');
  					  $this->db->from('tbl_pegawai');
					  $this->db->join('tbl_jabatan','tbl_jabatan.id_jabatan = tbl_pegawai.jabatan');
					  $this->db->join('tbl_golongan','tbl_golongan.id_pangkat = tbl_pegawai.golongan');
					  $this->db->join('tbl_bagian','tbl_bagian.id_bagian = tbl_pegawai.bagian');
					  $this->db->join('tbl_kateg_peg','tbl_kateg_peg.uuid_kateg_peg = tbl_pegawai.kateg_pegawai');
					  $this->db->join('tbl_pendidikan','tbl_pendidikan.id_pendidikan = tbl_pegawai.pendidikan');
					  $this->db->join('tbl_ptn','tbl_ptn.id_ptn = tbl_pegawai.ptn');
					  $this->db->join('tbl_agama','tbl_agama.id_agama = tbl_pegawai.agama');
					  $this->db->join('tbl_hobi','tbl_hobi.id_hobi = tbl_pegawai.hobi');
					  $this->db->join('tbl_kelurahan','tbl_kelurahan.id_kelurahan = tbl_pegawai.kelurahan');
					  $this->db->join('tbl_kecamatan','tbl_kecamatan.id_kecamatan = tbl_pegawai.kecamatan');
					  $this->db->join('absen_kateg_insentif_absen','absen_kateg_insentif_absen.absen_id_insentif_absen = tbl_pegawai.insentive_a');
					  $this->db->where('nik_ktp',$nik_ktp);
		$Q 			= $this->db->get();

    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
  		$this->db->close();
	}

	public function CekRekapAbsen()
	{
		$data = array();
  		$Q = $this->db->query("SELECT *, COUNT(absen_id_peg) as total FROM `absen_log_absensi` WHERE TIME(absen_datetime) >= '11:40:00' AND TIME(absen_datetime) <= '12:30:00' GROUP BY DATE(absen_datetime)");

    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
  		$this->db->close();
	}

	public function get_date_max_absen()
	{
		$data = array();
  		$Q = $this->db->query("SELECT max(absen_datetime) AS last_date FROM absen_log_absensi");

    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
  		$this->db->close();
	}

	public function getMaxOneData($maxfield,$table)
	{
		$data = array();
    	$Q = $this->db->query("SELECT * FROM ".$table." ORDER BY ".$maxfield." DESC LIMIT 1");

    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;

		$this->db->close();
	}

	public function getMaxOneDataWhere($field,$where,$maxfield,$table)
	{
		$data = array();
    	$Q = $this->db->query("SELECT * FROM ".$table." WHERE ".$field." = '".$where."' ORDER BY ".$maxfield." DESC LIMIT 1");

    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;

		$this->db->close();
	}

	public function getMaxOneData2Where($field,$where,$field2,$where2,$maxfield,$table)
	{
		$data = array();
    	$Q = $this->db->query("SELECT * FROM ".$table." WHERE ".$field." = '".$where."' AND ".$field2." = '".$where2."' ORDER BY ".$maxfield." DESC LIMIT 1");

    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;

		$this->db->close();
	}

	public function get_kinerja($nik_pegawai)
	{
		return $this->db->query("SELECT * FROM ekin_kinerja WHERE nik_pegawai ='".$nik_pegawai."' GROUP BY DATE(updated_at)");
		$this->db->close();
	}

	public function get_kinerja_filter($nik_pegawai,$tgl_awal,$tgl_akhir)
	{
		return $this->db->query("SELECT * FROM ekin_kinerja WHERE nik_pegawai ='".$nik_pegawai."' AND DATE(updated_at) >= '".$tgl_awal."' AND DATE(updated_at) <= '".$tgl_akhir."' GROUP BY DATE(updated_at)");
		$this->db->close();
	}

	public function get_kinerja_bawahan($nik_pegawai)
	{
		return $this->db->query("SELECT * FROM ekin_kinerja WHERE nik_pegawai ='".$nik_pegawai."'");
		$this->db->close();
	}

	public function get_kinerja_bawahan_noAcc($nik_pegawai)
	{
		return $this->db->query("SELECT * FROM ekin_kinerja WHERE nik_pegawai ='".$nik_pegawai."' AND acc_atasan = '0'");
		$this->db->close();
	}

	public function get_kinerja_bawahan_noAcc2($nik_atasan)
	{
		return $this->db->query("SELECT * FROM ekin_kinerja WHERE nik_atasan ='".$nik_atasan."' AND acc_atasan = '0'");
		$this->db->close();
	}

	public function get_kinerja_bawahan_All($nik_atasan,$date)
	{
		return $this->db->query("SELECT * FROM ekin_kinerja WHERE nik_atasan ='".$nik_atasan."' AND DATE(updated_at) = '".$date."'");
		$this->db->close();
	}

	public function get_kinerja_2($nik_pegawai,$date)
	{
		return $this->db->query("SELECT * FROM ekin_kinerja WHERE nik_pegawai ='".$nik_pegawai."' AND DATE(updated_at) = '".$date."'");
		$this->db->close();
	}

	public function get_history_wtp($wtp,$bulan)
	{
		return $this->db->query("SELECT * FROM `bar_monitor` WHERE monitor_wtp=".$wtp." AND MONTH(monitor_time)=".$bulan." ORDER BY monitor_no ASC");
		$this->db->close();
	}
	
	public function grafik_kinerja_1($date,$bagian)
	{
		return $this->db->query("SELECT 
			tbl_pegawai.nik_ktp,
		    tbl_pegawai.nama_pegawai,
		    tbl_bagian.bagian,
		    COUNT(nik_pegawai) AS jml_kinerja,
		    ekin_kinerja.updated_at
		FROM ekin_kinerja
		JOIN tbl_pegawai
		  ON ekin_kinerja.nik_pegawai = tbl_pegawai.nik_ktp
		JOIN tbl_bagian
		  ON tbl_bagian.id_bagian = tbl_pegawai.bagian
		WHERE DATE(updated_at) = '".$date."' AND id_bagian = '".$bagian."' GROUP BY nik_pegawai");
		$this->db->close();
	}

	public function get_warning_harian($tgl)
	{
		return $this->db->query("SELECT * FROM tbl_pegawai WHERE NOT EXISTS (SELECT * FROM ekin_kinerja WHERE date(updated_at)='".$tgl."' AND tbl_pegawai.nik_ktp = ekin_kinerja.nik_pegawai)");
		$this->db->close();
	}

	// KHUSUS UNTUK PERHITUNGAN ABSENSI

	public function getRekapAbsensi($kateg_pegawai,$from_date,$to_date)
	{
		return $this->db->query("SELECT no_urut_peg, nama_pegawai, id_absensi, current_insentive, absen_nominal_insentif_absen as upah_kerajinan, gaji, kateg_pegawai, date(absen_datetime), tbl_jabatan.jabatan, sum(case when ket='Masuk Pagi' then 1 else 0 end) as masuk_pagi, sum(case when ket='Pulang Siang' then 1 else 0 end) as pulang_siang, sum(case when ket='Masuk Siang' then 1 else 0 end) as masuk_siang, sum(case when ket='Pulang Sore' then 1 else 0 end) as pulang_sore, sum(case when ket='Masuk Pagi' then 1 when ket='Pulang Siang' then 1 when ket='Masuk Siang' then 1 when ket='Pulang Sore' then 1 else 0 end) as total FROM absen_log_absensi INNER JOIN tbl_pegawai ON absen_log_absensi.absen_id_peg=tbl_pegawai.id_absensi INNER JOIN tbl_jabatan ON tbl_pegawai.jabatan=tbl_jabatan.id_jabatan INNER JOIN absen_kateg_insentif_absen ON tbl_pegawai.insentive_a=absen_kateg_insentif_absen.absen_id_insentif_absen WHERE kateg_pegawai='".$kateg_pegawai."' AND date(absen_datetime) >= '".$from_date."' AND date(absen_datetime) <= '".$to_date."' GROUP BY absen_id_peg");
		$this->db->close();
	}

	public function getRekapAbsensi_pegawai($from_date,$to_date)
	{
		return $this->db->query("SELECT nik_ktp,no_urut_peg, nama_pegawai, id_absensi, current_insentive, absen_nominal_insentif_absen as upah_kerajinan, gaji, kateg_pegawai, date(absen_datetime), tbl_jabatan.jabatan, sum(case when ket='Masuk Pagi' then 1 else 0 end) as masuk_pagi, sum(case when ket='Pulang Siang' then 1 else 0 end) as pulang_siang, sum(case when ket='Masuk Siang' then 1 else 0 end) as masuk_siang, sum(case when ket='Pulang Sore' then 1 else 0 end) as pulang_sore, sum(case when ket='Masuk Pagi' then 1 when ket='Pulang Siang' then 1 when ket='Masuk Siang' then 1 when ket='Pulang Sore' then 1 else 0 end) as total FROM absen_log_absensi INNER JOIN tbl_pegawai ON absen_log_absensi.absen_id_peg=tbl_pegawai.id_absensi INNER JOIN tbl_jabatan ON tbl_pegawai.jabatan=tbl_jabatan.id_jabatan INNER JOIN absen_kateg_insentif_absen ON tbl_pegawai.insentive_a=absen_kateg_insentif_absen.absen_id_insentif_absen WHERE date(absen_datetime) >= '".$from_date."' AND date(absen_datetime) <= '".$to_date."' GROUP BY absen_id_peg");
		$this->db->close();
	}

	public function getRekapAbsensi_All($from_date,$to_date)
	{
		return $this->db->query("SELECT no_urut_peg, nama_pegawai, id_absensi, current_insentive, insentive_a, gaji, kateg_pegawai, date(absen_datetime), tbl_jabatan.jabatan, sum(case when ket='Masuk Pagi' then 1 else 0 end) as masuk_pagi, sum(case when ket='Pulang Siang' then 1 else 0 end) as pulang_siang, sum(case when ket='Masuk Siang' then 1 else 0 end) as masuk_siang, sum(case when ket='Pulang Sore' then 1 else 0 end) as pulang_sore, sum(case when ket='Masuk Pagi' then 1 when ket='Pulang Siang' then 1 when ket='Masuk Siang' then 1 when ket='Pulang Sore' then 1 else 0 end) as total FROM absen_log_absensi INNER JOIN tbl_pegawai ON absen_log_absensi.absen_id_peg=tbl_pegawai.id_absensi INNER JOIN tbl_jabatan ON tbl_pegawai.jabatan=tbl_jabatan.id_jabatan WHERE date(absen_datetime) >= '".$from_date."' AND date(absen_datetime) <= '".$to_date."' GROUP BY absen_id_peg");
		$this->db->close();
	}

	// END - KHUSUS UNTUK PERHITUNGAN ABSENSI









	// DATABASE 2 =========================================

	public function getWhereOneItem_db2($where,$field,$table)
	{
		$data = array();
  		$options = array($field => $where);
  		$Q = $this->db2->get_where($table,$options);
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;

  		$this->db2->close();
	}

	public function updateDataWhere_db2($where,$data,$table){
		$this->db2->where($where);
		$this->db2->update($table,$data);
		return;
		$this->db2->close();
	}

}